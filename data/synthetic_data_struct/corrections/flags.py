from .base_correction import BaseCorrection
import numpy as np 


class FlagsCorrection(BaseCorrection):
    
    
    def transform(self, df, df_schema=None):
        # TODO: ajouter une restricition aux colonnes effectivement présentes dans la table 
        # conditions in the form (var1, val1, var2, val2) meaning "if var1=val1, then var2=val2"
        conditions = [
            ("cmo_last", 1, "cmo", 1),
            ("cmo_first", 1, "cmo", 1),
            ("fullcode_first", 1, "fullcode", 1),
            ("dnr_first", 1, "dnr", 1),
            ("mort_icu", 1, "mort_hosp", 1),
            ("mort_hosp", 1, "hospital_expire_flag", 1),
            ("hospital_expire_flag", 1, "discharge_location", "DEAD/EXPIRED")
        ]

        for cond in conditions: 
            mask = (df[cond[0]]==cond[1])
            df.loc[mask, cond[2]] = cond[3]
        
        # NA imposition
        cols = ["cmo", "cmo_first", "cmo_last", "dnr", "dnr_first", "fullcode", "fullcode_first"]
        mask = df[cols].isna().any(axis=1)

        df.loc[mask, cols] = np.nan

        return df

        
        