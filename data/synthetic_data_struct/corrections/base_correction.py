import os
from abc import ABC, abstractmethod

import pandas as pd


class BaseCorrection(ABC):
    """Define a model for specific corrections"""

    def __init__(self, path_corrections_data: os.PathLike = None):
        pass
    
    def fit(self, correspondance_table: pd.DataFrame=None):
        """Structure information from the correspondance table
        Args:
            correspondance_table: dataframe with at least two columns , 'reference' and 'target'. Can also have a column 'relation' to specify the type of relation between the columns whose names are referenced in the 'reference' and 'target' columns.

        """
        if not correspondance_table is None:
            assert (
                "reference" in correspondance_table.columns
                and "target" in correspondance_table.columns
            )
        self.correspondance_table = correspondance_table

    @abstractmethod
    def transform(self, df: pd.DataFrame) -> pd.DataFrame:
        """Apply the correction to a given dataframe. Should output a corrected dataframe."""
        pass
