
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings('ignore')

# generic utility evaluation
from evaluation_functions.generic_signal_metrics.mutual_information import get_variation_information_matrix, get_variation_information_score
from evaluation_functions.generic_signal_metrics.pmf_comparison import get_hellinger_distance, get_pmfs

from evaluation_functions.visualization_functions.color_palette import PRIMARY_GREEN, PRIMARY_CT_GAN, PRIMARY_STRUCT

# prediction evaluation
from evaluation_functions.specific_signal_metrics.mortality_prediction import get_mortality_prediction

# generic privacy metrics
from evaluation_functions.generic_privacy_metrics.dcr_nndr import get_distance_to_closest, get_closest_distances_ratio

# specific privacy metric for avatar method

def compare_hellinger_distances(original: pd.DataFrame, avatar: pd.DataFrame, ct_gan: pd.DataFrame, structural: pd.DataFrame):
    """Compute hellinger distances for all variables and all synthetic data."""
    hellinger_distances_avatar = [
            get_hellinger_distance(*get_pmfs(original[col], avatar[col])) for col in original.columns
        ]
    hellinger_distances_ct_gan = [
            get_hellinger_distance(*get_pmfs(original[col], ct_gan[col])) for col in original.columns
        ]
    hellinger_distances_hdh = [
            get_hellinger_distance(*get_pmfs(original[col], structural[col])) for col in original.columns
        ]
    hellinger = hellinger_distances_avatar + hellinger_distances_ct_gan + hellinger_distances_hdh
    columns = list(original.columns)*3
    method = np.repeat(['Avatar', 'CT_GAN', 'Structural'], len(hellinger)/3)

    values = pd.DataFrame(zip(method, hellinger, columns), columns=['method', 'hellinger_distances', 'column_names'])

    return values


def plot_hellingers(values, variables):
    """Plot hellinger distances comparison."""
    fig, ax = plt.subplots(1, 1, figsize=(15, 10))
    sns.barplot(
        ax=ax,
        data=values[values['column_names'].isin(list(variables))],
        x="column_names",
        y='hellinger_distances',
        hue='method',
        palette=dict(Avatar=PRIMARY_GREEN, CT_GAN=PRIMARY_CT_GAN, Structural=PRIMARY_HDH),
    )

    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, horizontalalignment="right")
    ax.axhline(0.1, color="black", linestyle="--")
    ax.axhline(0.2, color="dimgrey", linestyle="--")

    ax.set_title('hellinger distance comparison')
    plt.show


def get_hellinger_statistics(hellinger, original, avatar, ct_gan, structural): 
    """Compute statistics on hellinger distances."""
    hellinger_mean_distance = []
    hellinger_std_distance = []
    hellinger_ratio_01 = []
    hellinger_ratio_02 = []
    variation_information = []
    method = ['Avatar', 'CT_GAN', 'Structural']
    synthetics = {'Avatar': avatar, 'CT_GAN': ct_gan, 'Structural': structural}
    for synthetic_data in method:
        
        print(f'\n ########### result {synthetic_data} ########### \n')
        hellinger_distances = hellinger[hellinger['method'] == synthetic_data]['hellinger_distances']

        hellinger_score = float(np.mean(hellinger_distances))
        hellinger_mean_distance.append(hellinger_score)
        print(f"The hellinger mean distance is {hellinger_score}")

        hellinger_std_distance.append(float(np.std(hellinger_distances)))
        print(f"The hellinger distance std is {float(np.std(hellinger_distances))}")

        ratio = [num for num in hellinger_distances if num < 0.1]
        ratio2 = [num for num in hellinger_distances if num < 0.2]

        ratio01 = len(ratio) / len(hellinger_distances)
        hellinger_ratio_01.append(ratio01)
        print(f"hellinger distance ratio < 0.1 {ratio01}")

        ratio02 = len(ratio2) / len(hellinger_distances)
        hellinger_ratio_02.append(ratio02)
        print(f"hellinger distance ratio < 0.2 {ratio02}")

        matrix1 = get_variation_information_matrix(original)
        matrix2 = get_variation_information_matrix(synthetics[synthetic_data])

        variation_information_score = get_variation_information_score(
            matrix1.values, matrix2.values
        )
        variation_information.append(variation_information_score)
        print(f"The variation of information score is {variation_information_score}")

    statistics = pd.DataFrame(zip(method,
        hellinger_mean_distance,
        hellinger_std_distance,
        hellinger_ratio_01,
        hellinger_ratio_02,
        variation_information), columns=['method',
        'hellinger_mean_distance',
        'hellinger_std_distance',
        'hellinger_ratio_01',
        'hellinger_ratio_02',
        'variation_information'])

    return statistics


def compare_mortality_predictions(original, avatar, ct_gan, structural):
    """Compute mortality prediction scores for all datasets."""
    auc = []
    f1_score = []
    accuracy = []
    method = []
    confusion_matrix = []

    prediction_data = {
        'original': original, 
        'avatar': avatar, 
        'ct_gan': ct_gan, 
        'structural': structural
    }
    for iter in range(10):
        for name, data in prediction_data.items(): 
            results = get_mortality_prediction(data)
            method.append(name)
            auc.append(results['auc'])
            accuracy.append(results['accuracy'])
            f1_score.append(results['f1'])
            confusion_matrix.append(results['confusion_matrix'])

    comparative_results = pd.DataFrame(zip(method, accuracy, f1_score, auc), columns =['method', 'accuracy', 'f1_score', 'auc'])
    return comparative_results, confusion_matrix

def compute_distance_metrics(coord_original, synthetic_coordinates):
    """Compute distance to closest and nearest distance ratio."""

    distance_to_closest = []
    closest_distances_ratio = []
    methods = []
    sd_method = ['avatar', 'structural', 'ct_gan']
    for method in sd_method:
        # DCR - distance to closest record
        distance_to_closest_tmp = get_distance_to_closest(
            coord_original, synthetic_coordinates[method]
        )

        # NNDR - nearest neighbor distance ratio
        closest_distances_ratio_tmp = get_closest_distances_ratio(
            coord_original, synthetic_coordinates[method]
        )

        distance_to_closest.append(pd.Series(distance_to_closest_tmp.values))
        closest_distances_ratio.append(pd.Series(closest_distances_ratio_tmp.values))
        methods = methods + [method for k in range(len(closest_distances_ratio_tmp.values))]


    # compute metrics on original values
    fractions = np.array([0.7, 0.3])
    original_target, original_holdout = np.array_split(coord_original,(fractions[:-1].cumsum()* len(coord_original)).astype(int)) 

    distance_to_closest_original = pd.Series(get_distance_to_closest(
        original_target, original_holdout
    ).values)
    closest_distances_ratio_original = pd.Series(get_closest_distances_ratio(
        original_target, original_holdout
    ).values)
    meth = pd.Series(["original" for k in range(len(distance_to_closest_original))])

    original_distances = pd.concat([distance_to_closest_original, closest_distances_ratio_original, meth], axis=1)

    

    distance_to_closest = pd.concat(distance_to_closest).reset_index(drop=True)
    closest_distances_ratio = pd.concat(closest_distances_ratio).reset_index(drop=True)
    methods = pd.Series(methods).reset_index(drop=True)

    distances_df  = pd.concat([distance_to_closest, closest_distances_ratio, methods], axis = 1)
    distances_df = pd.concat([distances_df, original_distances]).reset_index(drop=True)
    distances_df.columns = ['distance_to_closest', 'closest_distance_ratio', 'method']

    return distances_df

def prepare_dtypes(original: pd.DataFrame, structural: pd.DataFrame, avatar: pd.DataFrame, ct_gan: pd.DataFrame):
    VALUE = 10
    categorical_columns = []
    for col in original.columns:
        structural[col] = structural[col].astype(original[col].dtype)
    # transform to object some columns
    for col in original.columns:
        if len(np.unique(original[col])) < VALUE :
            original[col] = original[col].astype("object")
            avatar[col] = avatar[col].astype("object")
            ct_gan[col] = ct_gan[col].astype("object")
            structural[col] = structural[col].astype("object")
            categorical_columns.append(col)
    return original, structural, avatar, ct_gan