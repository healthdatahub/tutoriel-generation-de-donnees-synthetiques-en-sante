# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3.9.9 ('avatars-7Q8RRtkF-py3.9')
#     language: python
#     name: python3
# ---

import pandas as pd
import numpy as np
from ctgan import CTGANSynthesizer
from sdv.tabular import CTGAN

# +
original = pd.read_csv('../data/original/original_for_comparison.csv')

VALUE = 10
for col in original.columns:
    if len(np.unique(original[col])) < VALUE :
        original[col] = original[col].astype("object")


# +
model = CTGAN(epochs=1000, batch_size=600)
model.fit(original)

ctgan_data = model.sample(num_rows=(original.shape[0]))

# the newly generated synthetic ctgan dataset
# will be different than the one saved in the repo as seed is not configurable
ctgan_data.to_csv("../data/synthetic/ctgan_data.csv")

# -


