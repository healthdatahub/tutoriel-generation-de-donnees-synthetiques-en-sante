# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3.9.9 ('lsg-ZmCzlfd9-py3.9')
#     language: python
#     name: python3
# ---

import pandas as pd
import numpy as np

# +
SEED = 1
ID_COLS = ["subject_id"]

# needed if we want to filter the individuals dead before 30 hours (24+6)
GAP_TIME = 6  # In hours

# keep only the 24 first hours of each patient
WINDOW_SIZE = 24  # In hours
VITAL_LABS_PATH = "../data/base/vitals_labs.csv"
PATIENTS_PATH = "../data/base/patients.csv"

DATA_FILEPATH = "data/base/all_hourly_data.h5"

# feature selection inspired from
# https://www.nature.com/articles/s41598-022-11012-2
FEATURE_SAMPLE = [
    "hematocrit",
    "platelets",
    "creatinine",
    "glucose",
    "respiratory rate",
    "glascow coma scale total",
    "systolic blood pressure",
    "heart rate",
    "temperature",
    "partial pressure of oxygen",
    "white blood cell count",
    "sodium",
    "potassium",
]
# -

data_full_lvl2 = pd.read_csv(VITAL_LABS_PATH, header=[0, 1], index_col=[0, 1, 2, 3])
statics = pd.read_csv(PATIENTS_PATH, index_col=[0, 1, 2])
data_full_lvl2

statics.head()

# # Data manipulation and reduction
#  

# +
target_variables = statics[["mort_hosp", "mort_icu", "los_icu"]]
# if we want to filter individuals as in the paper:
# target_variables = statics[statics.max_hours > WINDOW_SIZE + GAP_TIME]
# [['mort_hosp', 'mort_icu', 'los_icu']]

target_variables["los_3"] = target_variables["los_icu"] > 3
target_variables["los_7"] = target_variables["los_icu"] > 7
target_variables.drop(columns=["los_icu"], inplace=True)
target_variables.astype(float)
print(target_variables.shape)
target_variables.head()

# +
# subset predictor variable
# subset through windows size
lvl2 = data_full_lvl2[
    (
        data_full_lvl2.index.get_level_values("subject_id").isin(
            set(target_variables.index.get_level_values("subject_id"))
        )
    )
    & (data_full_lvl2.index.get_level_values("hours_in") < WINDOW_SIZE)
]


lvl2_subj_idx, target_variables_subj_idx = [
    df.index.get_level_values("subject_id") for df in (lvl2, target_variables)
]
lvl2_hadm_idx, target_variables_hadm_idx = [
    df.index.get_level_values("hadm_id") for df in (lvl2, target_variables)
]
lvl2_icu_idx, target_variables_icu_idx = [
    df.index.get_level_values("icustay_id") for df in (lvl2, target_variables)
]

lvl2_subjects = set(lvl2_subj_idx)
lvl2_hadm = set(lvl2_hadm_idx)
lvl2_icu = set(lvl2_icu_idx)

assert lvl2_subjects == set(target_variables_subj_idx), "Subject ID pools differ!"
assert lvl2_hadm == set(target_variables_hadm_idx), "hadm ID pools differ!"
assert lvl2_icu == set(target_variables_icu_idx), "icustay ID pools differ!"


# -

def simple_imputer(df):
    idx = pd.IndexSlice
    df = df.copy()
    if len(df.columns.names) > 2:
        raise "`you must have 2 columns level"

    df_out = df.loc[:, idx[:, ["mean", "count"]]]

    # group by time periode
    df_out["time"] = df_out.index.get_level_values("hours_in").to_list()
    df_out["time"] = pd.cut(df_out["time"], 4, labels=[1, 2, 3, 4])

    mean_grouped = (
        df_out.groupby(["subject_id", "time"]).mean().loc[:, idx[:, ["mean"]]]
    )
    sum_grouped = df_out.groupby(["subject_id", "time"]).sum().loc[:, idx[:, ["count"]]]

    df_out = pd.merge(mean_grouped, sum_grouped, how="outer", on=["subject_id", "time"])

    icustay_means = df_out.loc[:, idx[:, "mean"]].groupby(ID_COLS).mean()
    # fill nan by the previous none nan values

    df_out.loc[:, idx[:, "mean"]] = (
        df_out.loc[:, idx[:, "mean"]].groupby(ID_COLS).fillna(method="ffill")
    )

    # if the previous value is nan, then fill nan by means or by zero if the mean is also na
    df_out.loc[:, idx[:, "mean"]] = (
        df_out.loc[:, idx[:, "mean"]].groupby(ID_COLS).fillna(icustay_means).fillna(0)
    )

    df_out.loc[:, idx[:, "count"]] = (df_out.loc[:, idx[:, "count"]] > 0).astype(
        "float"
    )
    df_out.rename(columns={"count": "mask"}, level="Aggregation Function", inplace=True)

    is_absent = 1 - df_out.loc[:, idx[:, "mask"]]
    hours_of_absence = is_absent.cumsum()
    time_since_measured = hours_of_absence - hours_of_absence[is_absent == 0].fillna(
        method="ffill"
    )
    time_since_measured.rename(
        columns={"mask": "time_since_measured"},
        level="Aggregation Function",
        inplace=True,
    )

    df_out = pd.concat((df_out, time_since_measured), axis=1)
    df_out.loc[:, idx[:, "time_since_measured"]] = df_out.loc[
        :, idx[:, "time_since_measured"]
    ].fillna(100)

    df_out.sort_index(axis=1, inplace=True)

    return df_out


# +
train_frac, dev_frac, test_frac = 0.7, 0.1, 0.2

np.random.seed(SEED)
# optional, standardize data
# idx = pd.IndexSlice
# lvl2_means = lvl2.loc[:, idx[:,'mean']].mean(axis=0)
# lvl2_stds = lvl2.loc[:, idx[:,'mean']].std(axis=0)
# lvl2.loc[:, idx[:,'mean']] = (lvl2.loc[:, idx[:,'mean']] - lvl2_means)/lvl2_stds

lvl2_imputed = simple_imputer(lvl2)

assert not lvl2_imputed.isnull().any().any(), "Imputation needed"
# -

lvl2_imputed.head()

# +
lvl2_imputed_flat = lvl2_imputed.pivot_table(index=["subject_id"], columns=["time"])

lvl2_imputed_flat

# +
variables = lvl2_imputed_flat.columns.get_level_values(0).unique()


[var for var in FEATURE_SAMPLE if var not in variables]

# +
idx = pd.IndexSlice

lvl2_feature_selection = lvl2_imputed_flat.loc[:, FEATURE_SAMPLE]
# -

lvl2_feature_selection.head()

raw = pd.merge(statics, lvl2_feature_selection, on="subject_id")
raw.reset_index(inplace=True)
raw.columns = [
    "_".join(str(x) for x in t) if isinstance(t, tuple) else t for t in raw.columns
]
raw

raw.to_csv("../data/original/flat_data.csv", index=False)


