
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score, roc_auc_score
from sklearn.preprocessing import OneHotEncoder
import pandas as pd

# Split train test
def get_mortality_prediction(mimic_df: pd.DataFrame):
    X = mimic_df.drop('mort_hosp',axis=1)
    y = mimic_df[['mort_hosp']]

    # encoding
    enc = OneHotEncoder(handle_unknown='ignore')
    X = enc.fit_transform(X)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, stratify=y)

    # Train Random forest
    rf_clf = RandomForestClassifier(criterion='entropy')   
    rf_clf.fit(X_train,y_train)

    # predict
    y_predict = rf_clf.predict(X_test)

    # performance metrics computation
    results = {'accuracy': accuracy_score(y_test,y_predict),
        'auc': roc_auc_score(y_test, y_predict),
        'f1': f1_score(y_test, y_predict),
        'confusion_matrix': confusion_matrix(y_test, y_predict)}

    return results