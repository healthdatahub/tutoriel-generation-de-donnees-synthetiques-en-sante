from typing import Any, List, NamedTuple, Tuple

import pandas as pd
import saiph

from evaluation_functions.specific_privacy_metrics.distance import get_avatar_closeness, get_nearest_neighbor

class GlobalHiddenRateResults(NamedTuple):
    """Store the results of the global hidden rate metric for intuitive access.

    Returns
    -------
        global_hidden_rate: percentage of individual having local cloaking > 0 at least once
        sensitive_individuals: index of records having aways local cloaking = 0 over iterations
    """

    global_hidden_rate: float
    sensitive_individuals: List[int]


def global_hidden_rate(
    records: pd.DataFrame,
    coord_records: pd.DataFrame,
    model: Tuple[Any],
    repeated_avatars: List[pd.DataFrame],
) -> GlobalHiddenRateResults:
    """Assess the overall ability of the method to ensure the safety of all individuals.

    We check that there is no individual producing the closest avatar to his data
    on all avatarizations.

    Arguments
    ---------
        records: original data with no missing data
        coord_records: projections of the original data
        model: saiph Model Class object
        repeated_avatars: results of repeated avatarizations with no missing data

    Returns
    -------
        results: the percentage of individuals well protected by the method (global hidden rate)
        and the indices of sensitive individuals.
    """
    for i, avatars in enumerate(repeated_avatars):
        coords_avatars = saiph.transform(avatars, model)

        indices_distances = get_nearest_neighbor(coord_records, coords_avatars)
        closeness = get_avatar_closeness(indices_distances)
        first_hit_index = [x for x in range(len(closeness)) if closeness[x] == 0]

        if i == 0:
            previous_index = first_hit_index
        else:
            # keep only index that were in the previous iteration
            previous_index = list(set(first_hit_index).intersection(previous_index))

    percentage: float = 100 - ((len(previous_index) / records.shape[0]) * 100)
    sensitive_individual = list(previous_index)
    results = GlobalHiddenRateResults(percentage, sensitive_individual)

    return results
