from typing import Any, List, Optional

import pandas as pd
import saiph

from evaluation_functions.specific_privacy_metrics.correlation_protection_rate import correlation_protection_rate
from evaluation_functions.specific_privacy_metrics.current_hidden_rate import current_hidden_rate
from evaluation_functions.specific_privacy_metrics.distance import (
    get_avatar_closeness,
    get_nearest_neighbor,
    get_unique_index,
)
from evaluation_functions.specific_privacy_metrics.global_hidden_rate import GlobalHiddenRateResults, global_hidden_rate
from evaluation_functions.specific_privacy_metrics.inference_metrics import InferenceMetricsResults, get_inference_metrics
from evaluation_functions.specific_privacy_metrics.local_cloaking import get_local_cloaking


class PrivacyMetrics:
    def fit(
        self,
        records: pd.DataFrame,
        avatars: pd.DataFrame,
        *,
        nf: int = 5,
        searching_frame: Optional[int] = None,
    ) -> None:
        """Fit the metrics class.

        Arguments
        ---------
            records: original data
            avatars: avatarized data
            nf: number of dimension used to fit privacy metrics
            searching_frame: number of neighbor per record to analyze
        """
        self._records = records
        self._avatars = avatars
        model = saiph.fit(records, nf=nf)
        self._model = model
        self._coord_original = saiph.transform(records, model)
        self._coord_avatar = saiph.transform(avatars, model)

        # Get list of nn indexes and distances between original and avatars
        indices_distances = get_nearest_neighbor(
            self._coord_original, self._coord_avatar, searching_frame=searching_frame
        )

        # Get index closeness of generated avatar for each record
        closeness = get_avatar_closeness(indices_distances)

        # Get index of unique avatars
        unique_index = get_unique_index(self._avatars)

        # Local cloaking
        self.local_cloaking = get_local_cloaking(indices_distances)

        # Hidden rate
        self.hidden_rate = current_hidden_rate(closeness, unique_index)

        # Hidden rate with attacker knowing local cloaking
        self.specific_hidden_rate = current_hidden_rate(
            closeness, unique_index, kth_hit=int(self.local_cloaking.avatars_median)
        )

    def correlation_protection_rate(self, variables: List[str]) -> float:
        """Apply the correlation protection rate metric.

        Arguments
        ---------
            variables: name of the variables known by the attacker
        """
        return correlation_protection_rate(
            self._records,
            self._avatars,
            current_hidden_rate=self.hidden_rate,
            variables=variables,
        )

    def inference_metrics(
        self, variables: List[str], target: str
    ) -> InferenceMetricsResults:
        """Apply the attack by inference metric.

        Arguments
        ---------
            variables: name of the variables known by the attacker
        """
        return get_inference_metrics(self._records, self._avatars, variables, target)

    def method_protection_rate(
        self,
        repeated_avatars: List[pd.DataFrame],
    ) -> GlobalHiddenRateResults:
        """Apply the attack by method_protection_rate metric.

        Arguments
        ---------
            iter: number of iteration to compute the metric
        """
        return global_hidden_rate(
            records=self._records,
            coord_records=self._coord_original,
            repeated_avatars=repeated_avatars,
            model=self._model,
        )
