import uuid
from typing import List

import pandas as pd

from evaluation_functions.utils.get_numeric_dtypes import split_columns_types

def correlation_protection_rate(
    records: pd.DataFrame,
    avatars: pd.DataFrame,
    current_hidden_rate: float,
    variables: List[str],
    boundary: float = 0.05,
) -> float:
    """Calculate the protection rate.

    Arguments
    ---------
        records
        avatars
        variable: variable names to consider
        current_hidden_rate: a float returned by the current_hidden_rate function
        boundary: percentage of variation for which we consider two values as identical

    Returns
    -------
        float: the proportion of records that could be considered "safe" in a probable scenario
    """
    if current_hidden_rate is None:
        raise ValueError("hidden_rate", "float within [0,100] needs to be specified")

    n2 = get_number_approximate_individuals(
        records.loc[:, variables], avatars.loc[:, variables], boundary
    )

    # Percentage of protection rate
    percentage: float = (
        1 - round(n2 * (1 - (current_hidden_rate / 100))) / len(records)
    ) * 100
    return percentage


def create_categorical(df: pd.DataFrame, value: str) -> pd.DataFrame:
    """Create a categorical variable using a value."""
    df["categorical"] = value
    df["categorical"] = df["categorical"].astype("category")

    return df


def get_number_approximate_individuals(
    records: pd.DataFrame, avatars: pd.DataFrame, boundary: float
) -> int:
    """Return the number of unique approximate avatars."""
    # Duplicated records are defined as protected under the GDPR
    unique_records = records.copy().drop_duplicates(ignore_index=True)

    # Get continuous and categorical variables
    continuous_index, categorial_index = split_columns_types(avatars)
    categorical_names = avatars.columns[categorial_index].tolist()
    continuous_names = avatars.columns[continuous_index].tolist()

    # Create fake categorical variable if needed
    if len(categorical_names) == 0:
        value = str(uuid.uuid4())
        categorical_names = ["categorical"]
        unique_records = create_categorical(unique_records, value)
        avatars = create_categorical(avatars, value)

    # Create id variables to count after joining
    unique_records["id"] = unique_records.index
    avatars["id"] = avatars.index

    # Join on categorical data
    joined_categorical = pd.merge(
        unique_records,
        avatars,
        on=categorical_names,
        how="inner",
        suffixes=("_original", "_avatar"),
    )

    tolerance = records[continuous_names].mean() * boundary
    col_original = [elem + "_original" for elem in continuous_names]
    col_avatar = [elem + "_avatar" for elem in continuous_names]

    # Select only continuous columns that need to be compared
    left = joined_categorical[col_original]
    left.columns = continuous_names
    right = joined_categorical[col_avatar]
    right.columns = continuous_names

    # Subset where np.nan are corresponding
    joined_categorical = joined_categorical[(left.isna() == right.isna()).all(axis=1)]

    # Fill nan by 0 (as they corresponding) to use pd.ge() and pd.le()
    left.fillna(0, inplace=True)
    right.fillna(0, inplace=True)

    # Subset rows by tolerance interval
    approximate = joined_categorical.loc[
        (left.ge(right - tolerance).all(axis=1))
        & (left.le(right + tolerance).all(axis=1)),
        :,
    ]
    # Groupby id, and count unique approximate avatars for each record
    count = approximate.groupby(["id_original"]).size()

    return sum(count == 1)
