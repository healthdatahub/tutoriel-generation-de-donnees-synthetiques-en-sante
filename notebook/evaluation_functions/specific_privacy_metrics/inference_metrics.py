import warnings
from typing import Any, List, NamedTuple, Optional

import numpy as np
import pandas as pd
import saiph
from numpy.typing import NDArray
from pandas.api.types import is_numeric_dtype
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    mean_squared_error,
    r2_score,
)

from evaluation_functions.utils.faiss_knn import FaissKNeighbors


class InferenceMetricsResults(NamedTuple):
    """Results of inference metrics using KNN algorithm.

    Returns
    -------
        prediction: predicted value using the KNN
        R2_score: Regression score (R squared).
        RMSE: Root Mean Square Error
        SMAPE: Symmetric mean absolute percentage error
        accuracy: Accuracy classification score
        confusion_matrix: Confusion matrix whose i-th row and j-th column
        entry indicates the number of samples with true label being i-th class
        and predicted label being j-th class.
    """

    prediction: NDArray[Any]
    R2_score: Optional[float]
    RMSE: Optional[float]
    SMAPE: Optional[float]
    accuracy: Optional[float]
    confusion_matrix: Optional[NDArray[np.int_]]


def get_inference_metrics(
    records: pd.DataFrame,
    avatars: pd.DataFrame,
    variable_known: List[str],
    target: str,
    k: int = 1,
    ncp: Optional[int] = None,
) -> InferenceMetricsResults:
    """Evaluate the performance of an attack by inference on an avatarized data set.

    The philosophy of this inference metrics is to evaluate the risk of good
    prediction using the nearest neighbor of each sensitive individual.

    We compute a knn with k=1 to evaluate this risk.
    Outputs metrics are commonly used metrics in machine learning.

    The metric calculated depends on the target type:

    - if target is continuous:
      - R2_score: coefficient of determination
      - RMSE: root-mean-square error
      - SMAPE: Symmetric mean absolute percentage error

    - if target is categorical:
      - acc: accuracy score
      - cf_mat: confusion matrix

    Arguments
    ---------
        records
        avatars
        variable_known: a list containing variables names known by the attacker
        target: The variable targeted by the attacker (should be numeric or categorical)
        k: number of neighbors used for knn, by default k = 1
        ncp: number of component chosen for data projection
          by default ncp = min(5, len(variable_known))

    Returns
    -------
        results: metrics calculated by the function, depending on target type
    """
    if not ncp:
        ncp = min(5, len(variable_known))

    if ncp > len(variable_known):
        warnings.warn(
            "Specifying ncp higher than number of variables known "
            "might lead to error if variables are continuous or with few "
            "modalities, please consider setting a lower number of ncp"
        )

    if avatars.shape[0] != records.shape[0]:
        raise ValueError(
            "dimension",
            "Records set and avatars set dataframes must have the same number of observations",
        )

    if not all(item in records.columns.tolist() for item in variable_known):
        raise ValueError("Data and avatar dataframes must have the same columns names")

    if target not in avatars.columns:
        raise ValueError("target", "target must be in records.columns")

    if not all(item in records.columns.tolist() for item in variable_known):
        raise ValueError("variable_known", "variable_known must be in records.columns")

    if target in variable_known:
        raise ValueError("Your target variable shouldn't be known.")

    if ncp > len(variable_known):
        raise ValueError(
            "ncp should be equal or lower to the number of known variables."
        )

    # Parameters
    avatar_known = avatars[variable_known]
    data_known = records[variable_known]
    avatar_target = avatars[target]

    # Create copy of records to work to avoid altering it in return
    working = records.copy()

    # Step 1 : Projection in the known space.
    # fit the space
    model = saiph.fit(avatar_known, nf=ncp)
    avatar_coord = saiph.transform(avatar_known, model)
    # Get individuals coordinates
    ori_coord = saiph.transform(data_known, model)

    # Get coordinates of nearest avatar(s) neighbor(s)
    nn = FaissKNeighbors(k=k)
    nn.fit(np.array(avatar_coord))

    # Save index neighbor(s)
    _, indexes = nn.predict(np.array(ori_coord))

    # Step 2 : predict using target type and compute metrics score
    # if target is numeric :
    if is_numeric_dtype(avatar_target):
        results = get_regression_metrics(working, avatars, target, indexes)
    # if target is categorical :
    else:
        results = get_classification_metrics(working, avatars, target, indexes)
    return results


def get_regression_metrics(
    records: pd.DataFrame,
    avatars: pd.DataFrame,
    target: str,
    indexes: NDArray[np.int_],
) -> InferenceMetricsResults:
    """Compute regression inference metrics.

    Arguments
    ---------
        records: original dataframe
        avatars: avatarized dataframe
        target: continuous target variable
        indexes: nearest neighbors indexes for each records
    """
    # estimate the target value
    predictions = [
        avatars.loc[neighbor_indexes, target].mean() for neighbor_indexes in indexes
    ]

    results = InferenceMetricsResults(
        prediction=np.array(predictions),
        R2_score=r2_score(records[target], predictions),
        RMSE=mean_squared_error(records[target], predictions),
        SMAPE=smape(np.array(predictions), records[target]),
        accuracy=None,
        confusion_matrix=None,
    )

    return results


def get_classification_metrics(
    records: pd.DataFrame,
    avatars: pd.DataFrame,
    target: str,
    indexes: NDArray[np.int_],
) -> InferenceMetricsResults:
    """Compute classification inference metrics.

    Arguments
    ---------
        records: original dataframe
        avatars: avatarized dataframe
        target: categorical target variable
        indexes: nearest neighbors indexes for each records
    """
    # Estimate the target value
    avatar_target = avatars[target]

    max_indexes: NDArray[np.int_] = np.array(
        [np.argmax(np.bincount(x), axis=-1) for x in indexes]
    )
    prediction = np.take_along_axis(
        avatar_target.values[:, None], max_indexes[:, None], axis=0
    )

    results = InferenceMetricsResults(
        prediction=prediction.flatten(),
        R2_score=None,
        RMSE=None,
        SMAPE=None,
        accuracy=accuracy_score(records[target], prediction),
        confusion_matrix=confusion_matrix(records[target], prediction),
    )

    return results


def smape(act: NDArray[np.float_], forc: NDArray[np.float_]) -> float:
    """Implement the symmetric mean absolute percentage error whose values are between 0 and 200%.

    If the calculated value and the original value are equal, the error is 0.
    """
    smape_result: float = (
        100 / len(act) * np.sum(2 * np.abs(forc - act) / (np.abs(act) + np.abs(forc)))
    )
    return smape_result
