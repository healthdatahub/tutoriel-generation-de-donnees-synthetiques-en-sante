from typing import Dict, Tuple

import numpy as np
import pandas as pd


def get_direct_match(
    records: pd.DataFrame, avatars: pd.DataFrame, sort_values: bool = False
) -> Tuple[float, str, Dict[str, float]]:
    """Evaluate the performance of an attack by direct univariate matching.

    It evaluates how likely a column is to be used as a direct identifier by considering both the
    percentage of direct matches and the number of unique values each variable has. This results in
    one value per column ranging from 0 (no risk of re-identification based on this column only)
    to 1 (the column can be used as a direct identifier).

    Arguments
    ---------
        records:
            original data
        avatars:
            avatars
        sort_values:
            set to `True` to sort each column before matching. This can be used on shuffled
            avatars. default = `False`.

    Returns
    -------
        maximum_match:
            the value of the highest direct matching score
        maximum_match:
            the name of the variable with the highest direct matching score
        all_match_values:
            the direct matching score of each variable
    """
    if avatars.shape[0] != records.shape[0]:
        raise ValueError(
            "Records set and avatars set dataframes must have the same number of observations",
        )

    all_raw_match: Dict[str, float] = {}
    all_match_values: Dict[str, float] = {}

    for col in avatars.columns:
        # Sort column values so that they are ordered in the same way.
        # If there are direct identifiers, similar values will be at the same index even
        # if the data was shuffled.
        if sort_values:
            originals_vals = records[col].sort_values()
            avatars_vals = avatars[col].sort_values()
        else:
            originals_vals = records[col]
            avatars_vals = avatars[col]

        # get number of direct matches
        matches = np.sum([x == y for x, y in zip(originals_vals, avatars_vals)])
        all_raw_match[col] = matches / len(records)

        # weight the percentage of direct matches based on number of unique values of the column
        all_match_values[col] = matches * len(set(avatars_vals)) / (len(records) ** 2)

    maximum_match_column: str = [
        key
        for key, value in all_match_values.items()
        if value == max(all_match_values.values())
    ][0]

    return (
        all_match_values[maximum_match_column],
        maximum_match_column,
        all_match_values,
    )
