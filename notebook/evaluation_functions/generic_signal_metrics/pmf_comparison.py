import warnings
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy.typing import NDArray
from pandas import DataFrame, Series

from evaluation_functions.utils.get_numeric_dtypes import NUMERIC_DTYPES
from evaluation_functions.generic_signal_metrics.mutual_information import get_series_to_epoch
from evaluation_functions.visualization_functions.color_palette import PRIMARY_GREEN, PRIMARY_GREY

def get_pmfs(
    var1: Series,
    var2: Series,
    nbins: int = 1,
    legend1: str = "Original variable",
    legend2: str = "Avatar variable",
    title: str = "PMFs comparison",
    figsize: Tuple[int, int] = (7, 7),
    plot: bool = False,
) -> Tuple[NDArray[np.float_], NDArray[np.float_]]:
    """Get the PMFs distributions of two variables.

    The Probability Mass Function (PMF) is an estimation
    of the variable distribution.

    Arguments
    ---------
        var1: the reference variable
        var2: the compared variable
        nbins: the number of bins for estimation of the PMF. If not
        specified, it's automatically calibrated
        legend1, legend2: legends for histogram plot
        title: tilte of the histogram
        figsize: size of the histogram figure

    Returns
    -------
        pmf1, pmf2: the two PMFs associated to the variables
    """
    check_same_dtypes(var1, var2)

    if str(var1.dtype).startswith("datetime"):
        var1 = get_series_to_epoch(var1)
        var2 = get_series_to_epoch(var2)

    # Compute bins range
    if var1.dtype in NUMERIC_DTYPES:
        range_min = np.minimum(np.min(var1), np.min(var2))
        range_max = np.maximum(np.max(var1), np.max(var2))

        # Ensure that ranges are finite
        range_min = np.nan_to_num(range_min)
        range_max = np.nan_to_num(range_max)

        if nbins == 1:
            scope = range_max - range_min
            scope = np.nan_to_num(scope)
            nbins = np.max([int(np.round(3 * scope / 4)), 20])
            nbins = np.minimum(nbins, 200)

        pmf1, _ = np.histogram(
            var1,
            bins=nbins,
            range=(range_min, range_max),
            density=True,
        )

        pmf2, _ = np.histogram(
            var2,
            bins=nbins,
            range=(range_min, range_max),
            density=True,
        )

        if plot:
            plot_pmfs(
                var1,
                var2,
                nbins=nbins,
                range_min=range_min,
                range_max=range_max,
                legend1=legend1,
                legend2=legend2,
                title=title,
                figsize=figsize,
            )

    else:  # Categorical variables

        original_var_unique = var1.unique()
        # Counts each original variables in the same order
        pmf1 = np.array(
            var1.value_counts(sort=False).reindex(original_var_unique).fillna(0)
        )
        pmf2 = np.array(
            var2.value_counts(sort=False).reindex(original_var_unique).fillna(0)
        )

        if plot:
            plt.figure(figsize=figsize)
            plt.subplot(1, 2, 1)
            sns.countplot(x=var1)
            plt.title(legend1)
            plt.subplot(1, 2, 2)
            sns.countplot(x=var2)
            plt.title(legend2)
            plt.show()

    # Normalize
    pmf1 = pmf1 / np.sum(pmf1)
    pmf2 = pmf2 / np.sum(pmf2)

    return pmf1, pmf2


def plot_pmfs(
    var1: Series,
    var2: Series,
    range_min: int,
    range_max: int,
    nbins: int,
    legend1: str = "Original variable",
    legend2: str = "Avatar variable",
    title: str = "PMFs comparison",
    figsize: Tuple[int, int] = (7, 7),
) -> None:
    """Plot PMFs of a given variable for original and avatar datasets.

    Arguments
    ---------
        Take the parameters of get_pmfs as input

    Returns
    -------
        Plot on the same figure the two PMFs

    """
    plt.figure(figsize=figsize)

    plt.hist(
        var1,
        bins=nbins,
        range=(range_min, range_max),
        density=True,
        histtype="stepfilled",
        color=PRIMARY_GREY,
        alpha=0.5,
    )

    plt.hist(
        var2,
        bins=nbins,
        range=(range_min, range_max),
        density=True,
        histtype="stepfilled",
        color=PRIMARY_GREEN,
        alpha=0.5,
    )

    plt.legend([legend1, legend2])
    plt.title(title)
    plt.show()


def get_kl_divergence(pmf1: NDArray[np.float_], pmf2: NDArray[np.float_]) -> float:
    """Compute KL divergence over two PMFs pmf1 and pmf2.

    It cannot handle 0 values, so corresponding rows are removed.

    Arguments
    ---------
        pmf1: PMF of a given variable in original dataset
        pmf2: PMF of the same variable in avatar dataset

    Returns
    -------
        kl_divergence: the KL divergence as a scalar
    """
    pmf1 = pmf1 / np.sum(pmf1)
    pmf2 = pmf2 / np.sum(pmf2)

    pmf1_positive_mask = pmf1 > 0
    pmf2_positive_mask = pmf2 > 0
    mask_zeros = pmf1_positive_mask * pmf2_positive_mask

    kl_divergence: float = np.sum(
        pmf1[mask_zeros] * np.log(pmf1[mask_zeros] / pmf2[mask_zeros])
    )

    return kl_divergence


def get_hellinger_distance(pmf1: NDArray[np.float_], pmf2: NDArray[np.float_]) -> float:
    """Compute Hellinger distance over two PMFs pmf1 and pmf2.

    Arguments
    ---------
        pmf1: PMF of a given variable in original dataset
        pmf2: PMF of the same variable in avatar dataset

    Returns
    -------
        hellinder_distance: the Hellinger distance as a scalar
    """
    pmf1 = pmf1 / np.sum(pmf1)
    pmf2 = pmf2 / np.sum(pmf2)

    hellinger_distance: float = np.sqrt(
        np.sum((np.sqrt(pmf1) - np.sqrt(pmf2)) ** 2)
    ) / np.sqrt(2)

    return hellinger_distance


def get_full_hellinger_distance(
    original: pd.DataFrame, avatars: pd.DataFrame
) -> Tuple[Dict[str, float], float, float]:
    hellinger_dict = {}
    for col in original.columns:
        check_same_dtypes(original[col], avatars[col])
        pmf1, pmf2 = get_pmfs(original[col].dropna(), avatars[col].dropna())
        hell_dist = get_hellinger_distance(pmf1, pmf2)
        hellinger_dict[col] = hell_dist
    hellinger_mean = np.array(list(hellinger_dict.values())).mean()
    hellinger_std = np.array(list(hellinger_dict.values())).std()
    return hellinger_dict, hellinger_mean, hellinger_std


def get_overlapping_ratio(pmf1: NDArray[np.float_], pmf2: NDArray[np.float_]) -> float:
    """Compute the overlapping ratio over two PMFs.

    Equals to PMF1 inter PMF2 over PMF1 union PM2. It's related to the
    mathematical notion of Total Variation, also known as statistical distance.

    Arguments
    ---------
        pmf1: PMF of a given variable in original dataset
        pmf2: PMF of the same variable in avatar dataset

    Returns
    -------
        ratio: a scalar between 0 and 1
    """
    union: float = np.sum(pmf1 + pmf2 + np.abs(pmf1 - pmf2)) / 2
    inter: float = np.sum(pmf1 + pmf2 - np.abs(pmf1 - pmf2)) / 2
    return inter / union


def get_pmf_score(
    dataset1: DataFrame,
    dataset2: DataFrame,
    hellinger_distance_threshold: float = 0.05,
    overlapping_ratio_threshold: float = 0.95,
    unique_values_coverage_threshold: float = 0.9,
    range_coverage_threshold: float = 0.9,
) -> Tuple[List[str], List[str]]:
    """Compute a score over all the variables.

    Returns variables that satisfied the contraints and those who don't.

    Arguments
    ---------
        dataset1: reference dataset (originals for instance)
        dataset2: dataset to compare (avatars for instance)
        hellinger_distance_threshold: threshold for hellinger distance (set to 1 to ignore)
        overlapping_ratio_threshold: threshold for overlapping ratio (set to 0 to ignore)
        unique_values_coverage_threshold: threshold for unique values coverage (set to 0 to ignore)
        range_coverage_threshold: threshold for range coverage (set to 0 to ignore)

    Returns
    -------
        var_similar: column names of the similar variables
        var_different: column names of the different variables
    """
    nb_observations_threshold = 500
    if dataset1.shape[0] < nb_observations_threshold:
        warnings.warn(
            f"""Small number of observations (<{nb_observations_threshold}).
            Consider a KS-test instead."""
        )

    var_similar: list[str] = []
    var_different: list[str] = []

    for col in dataset1.columns:

        pmf1, pmf2 = get_pmfs(dataset1[col], dataset2[col])
        hellinger_distance = get_hellinger_distance(pmf1, pmf2)
        overlapping_ratio = get_overlapping_ratio(pmf1, pmf2)
        unique_values_coverage = get_unique_values_coverage(
            dataset1[col], dataset2[col]
        )
        range_coverage = get_range_coverage(dataset1[col], dataset2[col])

        if np.isfinite(range_coverage):  # For numerical attributes
            if (
                hellinger_distance <= hellinger_distance_threshold
                and overlapping_ratio >= overlapping_ratio_threshold
                and unique_values_coverage >= unique_values_coverage_threshold
                and range_coverage >= range_coverage_threshold
            ):
                var_similar.append(col)
            else:
                var_different.append(col)

        else:  # For categorical attributes
            if (
                hellinger_distance <= hellinger_distance_threshold
                and overlapping_ratio >= overlapping_ratio_threshold
                and unique_values_coverage >= unique_values_coverage_threshold
            ):
                var_similar.append(col)
            else:
                var_different.append(col)

    return var_similar, var_different


def get_ecdf(pmf: NDArray[np.float_]) -> NDArray[np.float_]:
    """Get ECDF from PMF.

    ECDF stands for Empirical Cumulative Distribution Function.
    It's the cumulative sum of the Probability Mass Function (PMF)

    Arguments
    ---------
        pmf: the PMF of a given variable in original and avatar datasets

    Returns
    -------
        ecdf: the associated ECDF
    """
    ecdf = np.cumsum(pmf)
    return ecdf


def get_ks_test(
    var1: Series,
    var2: Series,
    alpha: float = 0.05,
) -> Tuple[float, float]:
    """Compute a two-sample Kolmogorov-Smirnov (KS) test.

    It's a statistical test to assess if two variables follow the same
    theoritical distribution. It's suited when we have a small number of
    observations, to determine if the differences in the variables PMFs
    are statistically significant or just due to the low statistical power
    of a small number of observations.

    Arguments
    ---------
        var1, var2: the two variable samples of a given variable in both datasets
        alpha: the significance level (default 0,05)

    Returns
    -------
        ks_statistic: the KS statistic
        critical_value: the critical value beyond which the test is rejected
    """
    pmf1, pmf2 = get_pmfs(var1, var2)
    ecdf1 = get_ecdf(pmf1)
    ecdf2 = get_ecdf(pmf2)

    ks_statistic = np.max(np.abs(ecdf1 - ecdf2))
    c_alpha = np.sqrt(-0.5 * np.log(alpha / 2))
    scale_factor = np.sqrt((len(var1) + len(var2)) / (len(var1) * len(var2)))
    critical_value = c_alpha * scale_factor

    return ks_statistic, critical_value


def get_unique_values_coverage(var1: Series, var2: Series) -> float:
    """Get the unique attribute values coverage ratio.

    Arguments
    ---------
        var1: the variable in the original dataset
        var2: the variable in the subset dataset

    Returns
    -------
        unique_values_coverage: the unique attribute values coverage ratio (between 0 and 1)
    """
    n_unique1: float = var1.nunique()
    n_unique2: float = var2.nunique()

    if n_unique1 == 0:
        return np.nan

    return n_unique2 / n_unique1


def get_range_coverage(var1: Series, var2: Series) -> float:
    """Get the range of the variable attribute coverage ratio.

    Arguments
    ---------
        var1: the variable in the original dataset
        var2: the variable in the subset dataset

    Returns
    -------
        range_coverage: the range coverage ratio (between 0 and 1)
    """
    if var1.dtype in NUMERIC_DTYPES:
        range_1: float = np.nan_to_num(np.max(var1) - np.min(var1))
        range_2: float = np.nan_to_num(np.max(var2) - np.min(var2))

        if range_1 == 0:
            return np.nan

        range_coverage: float = np.nan_to_num(range_2 / range_1)
        return range_coverage

    return np.nan


def get_average_metrics(
    dataset1: DataFrame, dataset2: DataFrame
) -> Tuple[float, float, float, float, float]:
    """Get the average utility metrics between two datasets.

    Arguments
    ---------
        dataset1: the original complete dataset
        dataset2: a subset of the original dataset

    Returns
    -------
        average_hellinger: the average hellinger distance
        average_overlapping: the average overlapping ratio
        average_range_coverage: the average range coverage for numerical variables only
        average_unique_values_coverage: the average unique values coverage ratio
        average_unique_categories_coverage: the average unique modalities coverage ratio
    """
    hellinger_distances: List[float] = []
    overlapping_ratios: List[float] = []
    unique_values_coverage: List[float] = []
    unique_categories_coverage: List[float] = []
    range_coverage: List[float] = []

    for col in dataset1.columns:
        pmf1, pmf2 = get_pmfs(
            dataset1[col],
            dataset2[col],
            plot=False,
        )

        hellinger_distances.append(get_hellinger_distance(pmf1, pmf2))
        overlapping_ratios.append(get_overlapping_ratio(pmf1, pmf2))

        if dataset1[col].dtype in NUMERIC_DTYPES:
            unique_values_coverage.append(
                get_unique_values_coverage(dataset1[col], dataset2[col])
            )
            range_coverage.append(get_range_coverage(dataset1[col], dataset2[col]))
        else:
            unique_categories_coverage.append(
                get_unique_values_coverage(dataset1[col], dataset2[col])
            )

    return (
        np.mean(pd.Series(hellinger_distances).dropna()),
        np.mean(pd.Series(overlapping_ratios).dropna()),
        np.mean(pd.Series(unique_values_coverage).dropna()),
        np.mean(pd.Series(unique_categories_coverage).dropna()),
        np.mean(pd.Series(range_coverage).dropna()),
    )


def check_same_dtypes(serie1: pd.Series, serie2: pd.Series) -> None:
    if serie1.dtype != serie2.dtype:
        raise ValueError(
            f"Expected column {serie2.name} to have the same data type,"
            f"got {serie1.dtype} and {serie2.dtype} instead."
        )
