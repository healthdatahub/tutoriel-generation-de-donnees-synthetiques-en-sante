"""Computes the mutual information and variation of information metrics.

The mutual information between two probability density functions of two
variables measures how much one variable is explained by the other one.

The normalized variation of information, on the contrary, measures how much the
variables are complementary, and is a metric bounded in [0;1].
"""


import numpy as np
import pandas as pd
from numpy.typing import NDArray
from pandas import DataFrame, Series
from typing import Tuple

from evaluation_functions.utils.get_numeric_dtypes import NUMERIC_DTYPES


def get_mutual_information_matrix(df: DataFrame) -> DataFrame:
    """Compute the mutual information matrix across all pairs of variables.

    The lower triangle matrix is computed only, because the matrix
    is symmetric.

    If you aim to compare some mutual information matrix, you may consider
    using the variation of information matrix instead (the variation of information
    is a metric bounded in [0,1], mutual information is not a metric).

    See also: https://en.wikipedia.org/wiki/Mutual_information

    Arguments
    ---------
        df: the dataset

    Returns
    -------
        mutual_information_matrix: the matrix of mutual information
    """
    nb_variables = len(df.columns)
    columns = df.columns
    mutual_information_matrix = np.zeros((nb_variables, nb_variables))
    for i in range(0, nb_variables):
        for j in range(0, i):
            joint_pmf = get_joint_pmf(df[columns[i]], df[columns[j]])
            mutual_information_matrix[i, j] = get_mutual_information(joint_pmf)

    return DataFrame(mutual_information_matrix, columns=columns, index=columns)


def get_variation_information_matrix(df: DataFrame) -> DataFrame:
    """Compute the variation of information matrix across all pairs of variables.

    The lower triangle matrix is computed only, because the matrix
    is symmetric.

    See also: https://en.wikipedia.org/wiki/Mutual_information

    Arguments
    ---------
        df: the dataset

    Returns
    -------
        variation_information_matrix: matrix of variation of information
    """
    nb_variables = len(df.columns)
    columns = df.columns
    variation_information_matrix = np.zeros((nb_variables, nb_variables))
    for i in range(0, nb_variables):
        for j in range(0, i):
            joint_pmf = get_joint_pmf(df[columns[i]], df[columns[j]])
            variation_information_matrix[i, j] = get_variation_information(joint_pmf)

    return DataFrame(variation_information_matrix, columns=columns, index=columns)


def get_variation_information_score(
    matrix1: pd.DataFrame,
    matrix2: pd.DataFrame,
) -> float:
    """Compute the variation of information score.

    It's defined as the average absolute difference between the variation of information
    matrices.
    It's bounded in [0;1] (lower, better)
    Target: 0

    See also: https://en.wikipedia.org/wiki/Mutual_information

    Arguments
    ---------
        matrix1: the variation of information matrix of the
            reference dataset
        matrix2: the variation of information matrix of the
            dataset to compare

    Returns
    -------
        variation_information_score: the variation of information score
    """
    nb_non_zero = (
        matrix1.shape[0] * (matrix1.shape[0] - 1) / 2
    )  # Number of non-zero variation of information elements in the lower triangle
    differences = np.abs(matrix1 - matrix2)
    variation_information_score: float = np.sum(np.sum(differences)) / nb_non_zero
    return variation_information_score


def get_mutual_information(joint_pmf: NDArray[np.float_]) -> float:
    """Compute the mutual information between two variables.

    The mutual information of variables X and Y is defined as the
    Kullback-Leibler divergence between the cojoint PMFs pmf(X,Y) and
    the product of the two marginals PMF pmf(X) and pmf(Y).

    It's a general measure of the dependence betweeen two variables,
    while Pearson correlation coefficient measures only linear dependences
    between numerical variables.

    The mutual information between two variables is calculated thanks to the
    joint distribution of the two variables (joint_pmf or joint_pdf).

    If you aim to compare some mutual information, you may consider using the
    normalized variation of information instead, which is a metric related to
    the mutual information through a linear expression (the mutual information
    is not a metric).

    See also: https://en.wikipedia.org/wiki/Mutual_information

    Arguments
    ---------
        joint_pmf: the joint PMF p(x,y)

    Returns
    -------
        mutual_information: the mutual information scalar
    """
    if float(np.sum(joint_pmf)) != 0:
        pxy = joint_pmf / float(np.sum(joint_pmf))
    else:
        pxy = joint_pmf.copy()

    positive_mask = pxy > 0

    px: NDArray[np.float_] = np.sum(pxy, axis=1)  # marginal for x over y
    py: NDArray[np.float_] = np.sum(pxy, axis=0)  # marginal for y over x
    px_py: NDArray[np.float_] = px[:, np.newaxis] * py[np.newaxis, :]

    if len(pxy[positive_mask]) == 0 and len(px_py[positive_mask]):
        mutual_information: float = float(0)
    else:
        mutual_information = np.sum(
            pxy[positive_mask] * np.log(pxy[positive_mask] / px_py[positive_mask])
        )
    return mutual_information


def get_variation_information(joint_pmf: NDArray[np.float_]) -> float:
    """Compute the normalized variation of information.

    The normalized variation of information is a metric (mutual information is not),
    bounded in [0,1], which is linked to the mutual information through a linear
    expression. For this reason, it's simpler to interpret.

    VI = 1 - (MI(X,Y) / H(X,Y))
    Bounded in [0,1].
    VI=0 means that variables X and Y are completely dependent
    (X is completely explained by variable Y, and vice-versa).
    VI=1 means that variables X and Y are independent.

    See also: https://en.wikipedia.org/wiki/Mutual_information

    Arguments
    ---------
        joint_pmf: the joint PMF p(x,y)

    Returns
    -------
        variation_information: the normalized variation of information
    """
    if float(np.sum(joint_pmf)) != 0:
        pxy = joint_pmf / float(np.sum(joint_pmf))
    else:
        pxy = joint_pmf.copy()
    entropy = get_entropy(pxy)
    mutual_information = get_mutual_information(pxy)
    if entropy == 0:
        variation_information = float(0)
    else:
        variation_information = 1 - (mutual_information / entropy)

    return variation_information


def get_joint_pmf(var1: Series, var2: Series, nbins: int = 20) -> NDArray[np.float_]:
    """Compute the joint PMF of two variables p(x,y).

    PMF: probability mass function. It's used to estimate the actual
    distribution of a variable. The joint PMF is then an estimation of
    the cojoint distribution of variables X and Y.

    The joint_pmf is used to compute the mutual information between two variables,
    as well as the variation of information.

    See also: https://en.wikipedia.org/wiki/Joint_probability_distribution

    Arguments
    ---------
        var1: x
        var2: y
        nbins: the number of bins for estimation of the joint PMF

    Returns
    -------
        joint_pmf: the joint PMF p(x,y)
    """
    # Remove NANs
    mask1 = pd.notnull(var1)
    mask2 = pd.notnull(var2)
    mask = mask1 * mask2
    var1 = var1[mask]
    var2 = var2[mask]

    if str(var1.dtype).startswith("datetime"):
        var1 = get_series_to_epoch(var1)

    if str(var2.dtype).startswith("datetime"):
        var2 = get_series_to_epoch(var2)

    if var1.dtype not in NUMERIC_DTYPES and var2.dtype in NUMERIC_DTYPES:
        var1_unique = var1.unique()
        joint_pmf = np.zeros((var1_unique.shape[0], nbins))
        for i in range(0, var1_unique.shape[0]):
            mask = var1 == var1_unique[i]

            # Compute the PMF of var2 given var1
            joint_pmf[i, :], _ = np.histogram(var2[mask], bins=nbins)

    elif var2.dtype not in NUMERIC_DTYPES and var1.dtype in NUMERIC_DTYPES:
        var2_unique = var2.unique()
        joint_pmf = np.zeros((nbins, var2_unique.shape[0]))
        for j in range(0, var2_unique.shape[0]):
            mask = var2 == var2_unique[j]

            # Compute the PMF of var1 given var2
            joint_pmf[:, j], _ = np.histogram(var1[mask], bins=nbins)

    elif var1.dtype not in NUMERIC_DTYPES and var2.dtype not in NUMERIC_DTYPES:
        # Save the categories to count them even if it's 0
        var1_unique = var1.unique()
        var2_unique = var2.unique()

        joint_pmf = np.zeros((var1_unique.shape[0], var2_unique.shape[0]))
        for i in range(0, var1_unique.shape[0]):
            mask = var1 == var1_unique[i]

            # Compute the PMF of var2 given var1
            joint_pmf[i, :] = (
                var2[mask].value_counts(sort=False).reindex(var2_unique).fillna(0)
            )

    else:  # Numerical attributes only
        joint_pmf, _, _ = np.histogram2d(x=var1, y=var2, bins=nbins)

    # Normalize
    if float(np.sum(joint_pmf)) != 0:
        joint_pmf = joint_pmf / float(np.sum(joint_pmf))

    return joint_pmf


def get_entropy(pmf: Series) -> float:
    """Compute the entropy of a variable.

    See also: https://en.wikipedia.org/wiki/Entropy_(information_theory)

    Arguments
    ---------
        pmf: the PMF of the variable:
            p(x) the PMF of the variable x
            OR p(x,y) the joint PMF of the variables (x,y)

    Returns
    -------
        entropy: the entropy of the variable:
            if p(x), returns the entropy H(X)
            if p(x,y), returns the joint entropy H(X,Y)
    """
    if float(np.sum(pmf)) != 0:
        pmf = pmf / float(np.sum(pmf))
    elementary_entropies = pmf * np.log(pmf, where=pmf > 0)
    np.nan_to_num(elementary_entropies, copy=False, nan=0.0, posinf=None, neginf=None)
    entropy: float = -np.sum(elementary_entropies)
    return entropy


def get_conditional_entropy(
    joint_pmf: NDArray[np.float_], given_x: bool = True
) -> float:
    """Compute the conditional entropy of y given x or vice-versa.

    See also: https://en.wikipedia.org/wiki/Entropy_(information_theory)

    Arguments
    ---------
        joint_pmf: the joint PMF p(X,Y)
        given_x: boolean, compute H(Y|X) if true, H(X|Y) else

    Returns
    -------
        conditional_entropy: the conditional entropy H(Y|X) or H(X|Y)
    """
    if float(np.sum(joint_pmf)) != 0:
        pxy = joint_pmf / float(np.sum(joint_pmf))
    else:
        pxy = joint_pmf.copy()

    # axis=1 -> px (H(Y|X)), axis=0 -> py (H(X|Y))
    axis = 1 if given_x else 0

    marginal_probability: NDArray[np.float_] = np.sum(pxy, axis=axis)
    elementary_conditional_entropies = pxy * np.log(
        pxy / marginal_probability, where=pxy > 0
    )
    conditional_entropy: float = -np.sum(elementary_conditional_entropies)

    return conditional_entropy


def get_series_to_epoch(var: Series) -> Series:
    """Transform a datetime series into time post epoch series.

    Arguments
    ---------
        var: pandas series to transform

    Returns
    -------
        var: transformed series
    """
    epoch = np.datetime64(0, "Y")  # 0 years since epoch ==> epoch itself
    var = (var - epoch) / np.timedelta64(1, "s")
    var = var.astype(float)
    return var

def get_correlation_difference_ratio(
    original_corr: DataFrame, synth_corr: DataFrame
) -> Tuple[DataFrame, float]:
    """Compute the correlation differences as a percentage.

    Arguments
    ---------
        original_corr: Pearson correlation matrix for original dataset
        synth_corr: Pearson correlation matrix for synthetic dataset

    Returns
    -------
        diff_ratio_per_variable: DataFrame with the difference per variable in percent
        average_diff_ratio: the average difference in percent
    """
    nb_rows: int = original_corr.shape[0]
    nb_cols: int = original_corr.shape[1]

    abs_diff: DataFrame = np.abs(original_corr - synth_corr)
    diff_ratio_per_variable: DataFrame = 100 * abs_diff / 2

    average_diff_ratio: float = np.sum(np.sum(diff_ratio_per_variable)) / (
        nb_rows * (nb_cols - 1)
    )

    return diff_ratio_per_variable, average_diff_ratio