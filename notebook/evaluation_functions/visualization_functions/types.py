from enum import Enum


class DataType(Enum):
    ORIGINAL = "original"
    AVATAR = "avatar"
    BOTH = "both"


class ProjectionSpace(Enum):
    ORIGINAL = "original"
    COMMON = "common"


ORIGINAL_LABEL = "Original"
AVATAR_LABEL = "Avatar"
SYNTHETIC_LABEL = "Synthetic"
