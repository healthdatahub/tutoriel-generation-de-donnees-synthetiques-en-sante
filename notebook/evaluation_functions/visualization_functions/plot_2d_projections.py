from pathlib import Path
from typing import Optional, Tuple

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from evaluation_functions.visualization_functions.color_palette import PRIMARY_GREEN, PRIMARY_GREY
from evaluation_functions.visualization_functions.types import (
    SYNTHETIC_LABEL,
    ORIGINAL_LABEL,
    ProjectionSpace,
)


def plot_2d_projections(
    records_coordinates: Optional[pd.DataFrame] = None,
    synthetic_coordinates: Optional[pd.DataFrame] = None,
    *,
    projection_space: ProjectionSpace = ProjectionSpace.ORIGINAL,
    figsize: Tuple[int, int] = (14, 8),
    original_color: str = PRIMARY_GREY,
    synthetic_color: str = PRIMARY_GREEN,
    compare_synthetic: bool = False, 
    name: str = "synthetic data"
) -> matplotlib.figure.Figure:
    """Project either original, synthetic or both in the same dimensional space.

    Arguments
    ---------
        records_coordinates:  coordinates of original records
        synthetic: coordinates of avatar records
        projection_space:  space for the projection {default: original}
        figsize: vector of two int specifying figure size
    Returns
    -------
        fig: a plot of the projected records and/or synthetic in a 2D projection space.
    """
    if records_coordinates is None and synthetic_coordinates is None:
        raise ValueError(
            "Expected at least one of 'records_coordinates' or 'synthetic_coordinates'."
        )

    # project in common space
    if projection_space == ProjectionSpace.COMMON:
        title_suffix = "common euclidean space"

    # project in original space
    elif projection_space == ProjectionSpace.ORIGINAL:
        title_suffix = "original euclidean space"

    else:
        raise ValueError(
            "Expected a type of ProjectionSpace for projection_space, "
            f"got {type(projection_space)} instead."
        )

    if records_coordinates is not None and synthetic_coordinates is not None:
        if not compare_synthetic:
            label_1 = ORIGINAL_LABEL
            label_2 = name
        else:
            label_1 = 'Synthetic_1'
            label_2 = 'Synthetic_2'
        coordinates = pd.concat(
            [
                records_coordinates.assign(Type=label_1),
                synthetic_coordinates.assign(Type=label_2),
            ],
            ignore_index=True,
        )
        title = (
            f"Comparative projection of {label_1} "
            f"and {label_2} data in {title_suffix}"
        )
    elif records_coordinates is not None:
        coordinates = records_coordinates.assign(Type=label_1)
        title = f"Projection of {label_1} data in {title_suffix}"
    elif synthetic_coordinates is not None:
        coordinates = synthetic_coordinates.assign(Type=label_2)
        title = f"Projection of {label_2} data in {title_suffix}"

    fig, ax = plt.subplots(1, 1, figsize=figsize)
    sns.scatterplot(
        ax=ax,
        x="Dim. 1",
        y="Dim. 2",
        data=coordinates,
        hue="Type",
        alpha=0.6,
        palette={label_1: original_color, label_2: synthetic_color},
    )
    ax.set_ylim(
        coordinates["Dim. 2"].min() - 0.2,
        coordinates["Dim. 2"].max() + 0.2,
    )
    ax.set_xlim(
        coordinates["Dim. 1"].min() - 0.2,
        coordinates["Dim. 1"].max() + 0.2,
    )
    ax.set_title(title)

    fig.tight_layout()
    return fig
