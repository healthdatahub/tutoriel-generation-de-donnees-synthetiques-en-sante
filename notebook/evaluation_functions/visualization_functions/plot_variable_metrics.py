import random
from typing import Dict, Tuple

import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

from evaluation_functions.visualization_functions.color_palette import PRIMARY_GREEN, PRIMARY_GREY


def plot_variable_metrics(
    metric_per_variable: Dict[str, float],
    objectif_threshold: float = 0.05,
    tolerance_threshold: float = 0.1,
    title: str = "Hellinger distance per variable",
    figsize: Tuple[int, int] = (15, 10),
) -> matplotlib.figure.Figure:
    """Plot the given metric per variable.

    Arguments
    ---------
        metric_per_variable: a given metric for each variable
        objectif_threshold: threshold for metrics that fulfilled the objective. Set to None
            to remove this information from the plot. default: 0.05
        tolerance_threshold: threshold for metrics that don't fulfilled the objectif, but
            not too far. Set to None to remove this information from the plot. default: 0.1

        figsize: figure dimension.

    """
    fig, ax = plt.subplots(1, 1, figsize=figsize)

    sns.barplot(
        ax=ax,
        x=list(metric_per_variable.keys()),
        y=list(metric_per_variable.values()),
        color=PRIMARY_GREEN,
    )

    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, horizontalalignment="right")
    if objectif_threshold:
        ax.axhline(objectif_threshold, color="black", linestyle="--")
    if tolerance_threshold:
        ax.axhline(tolerance_threshold, color=PRIMARY_GREY, linestyle="--")

    ax.set_title(title)

    return fig
