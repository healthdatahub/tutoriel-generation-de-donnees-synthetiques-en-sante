from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import saiph
import seaborn as sns
from numpy.typing import NDArray

from evaluation_functions.visualization_functions.color_palette import PRIMARY_GREEN, PRIMARY_GREY


def individual_travel_plot(
    records: pd.DataFrame,
    avatars: pd.DataFrame,
    indiv: int,
    figsize: Tuple[int, int] = (14, 8),
    projection_space: str = "original",
) -> plt.Axes:
    """Visualize the outcome of one individual.

    Arguments
    ---------
        records: records
        avatars: avatars
        indiv: int values specifying the index of the individual to be focused on
        projection_space:  string specifying the space of projection {default: original}
        style: string specifying seaborn theme used
        figsize: vector of two int specifying figure size

    Returns
    -------
        graph: a 2 dimensional representation of record to avatar travel
    """
    # project in common space
    if projection_space == "common":
        total = pd.concat([records, avatars])
        total_coordinates, _ = saiph.fit_transform(total, nf=3)

    # project in original space
    else:
        model = saiph.fit(records, nf=3)
        ori_coord = saiph.transform(records, model)
        ava_coord = saiph.transform(avatars, model)
        total_coordinates = pd.concat([ori_coord, ava_coord])
    x: NDArray[np.str_] = np.array(["Original", "Avatar"])
    label = np.repeat(x, [len(records), len(avatars)], axis=0)
    total_coordinates["Type"] = label

    original_coord = total_coordinates.loc[
        total_coordinates["Type"] == "Original"
    ].reset_index(drop=True)
    avatar_coord = total_coordinates.loc[
        total_coordinates["Type"] == "Avatar"
    ].reset_index(drop=True)

    avatar_among_ori = pd.concat(
        [original_coord, pd.DataFrame(avatar_coord.iloc[indiv, :]).transpose()]
    )
    avatar_among_ori["index"] = avatar_among_ori.index
    ori_among_avatar = pd.concat(
        [pd.DataFrame(original_coord.iloc[indiv, :]).transpose(), avatar_coord]
    )
    ori_among_avatar["index"] = ori_among_avatar.index
    ori_to_ava = pd.concat(
        [
            pd.DataFrame(original_coord.iloc[indiv, :]).transpose(),
            pd.DataFrame(avatar_coord.iloc[indiv, :]).transpose(),
        ]
    )
    ori_to_ava["index"] = ori_to_ava.index

    for i in range(len(avatar_among_ori)):
        if (
            avatar_among_ori.iloc[i, (avatar_among_ori.shape[1] - 1)] == indiv
            and avatar_among_ori.iloc[i, (avatar_among_ori.shape[1] - 2)] == "Original"
        ):
            avatar_among_ori.iloc[i, avatar_among_ori.shape[1] - 2] = "Indiv_ori"
        if avatar_among_ori.iloc[i, (avatar_among_ori.shape[1] - 2)] == "Avatar":
            avatar_among_ori.iloc[i, avatar_among_ori.shape[1] - 2] = "Indiv_ava"

    for i in range(len(ori_among_avatar)):
        if ori_among_avatar.iloc[i, (ori_among_avatar.shape[1] - 2)] == "Original":
            ori_among_avatar.iloc[i, ori_among_avatar.shape[1] - 2] = "Indiv_ori"
        if (
            ori_among_avatar.iloc[i, (ori_among_avatar.shape[1] - 1)] == indiv
            and ori_among_avatar.iloc[i, (ori_among_avatar.shape[1] - 2)] == "Avatar"
        ):
            ori_among_avatar.iloc[i, ori_among_avatar.shape[1] - 2] = "Indiv_ava"

    fig = plt.figure(figsize=figsize)
    ax, ax1, ax2 = fig.subplots(1, 3)

    sns.scatterplot(
        ax=ax,
        x="Dim. 1",
        y="Dim. 2",
        data=avatar_among_ori,
        hue="Type",
        alpha=0.7,
        palette=dict(Original=PRIMARY_GREY, Indiv_ori="black", Indiv_ava=PRIMARY_GREEN),
    )
    ax.set_xlim(
        total_coordinates["Dim. 1"].min() - 0.2, total_coordinates["Dim. 1"].max() + 0.2
    )
    ax.set_ylim(
        total_coordinates["Dim. 2"].min() - 0.2, total_coordinates["Dim. 2"].max() + 0.2
    )

    x1 = avatar_among_ori.loc[avatar_among_ori["Type"] == "Indiv_ori"]["Dim. 1"].values[
        0
    ]
    x2 = avatar_among_ori.loc[avatar_among_ori["Type"] == "Indiv_ava"]["Dim. 1"].values[
        0
    ]
    y1 = avatar_among_ori.loc[avatar_among_ori["Type"] == "Indiv_ori"]["Dim. 2"].values[
        0
    ]
    y2 = avatar_among_ori.loc[avatar_among_ori["Type"] == "Indiv_ava"]["Dim. 2"].values[
        0
    ]

    ax.arrow(
        x1,
        y1,
        x2 - x1,
        y2 - y1,
        width=0.01,
        color=PRIMARY_GREEN,
        head_length=0.0,
        head_width=0.0,
    )
    ax.set_title("Avatar among Original values")

    sns.scatterplot(
        ax=ax1,
        x="Dim. 1",
        y="Dim. 2",
        data=ori_among_avatar,
        hue="Type",
        alpha=0.7,
        palette=dict(Avatar=PRIMARY_GREEN, Indiv_ori=PRIMARY_GREY, Indiv_ava="magenta"),
    )
    ax1.set_xlim(
        total_coordinates["Dim. 1"].min() - 0.2, total_coordinates["Dim. 1"].max() + 0.2
    )
    ax1.set_ylim(
        total_coordinates["Dim. 2"].min() - 0.2, total_coordinates["Dim. 2"].max() + 0.2
    )

    x1 = ori_among_avatar.loc[ori_among_avatar["Type"] == "Indiv_ori"]["Dim. 1"].values[
        0
    ]
    x2 = ori_among_avatar.loc[ori_among_avatar["Type"] == "Indiv_ava"]["Dim. 1"].values[
        0
    ]
    y1 = ori_among_avatar.loc[ori_among_avatar["Type"] == "Indiv_ori"]["Dim. 2"].values[
        0
    ]
    y2 = ori_among_avatar.loc[ori_among_avatar["Type"] == "Indiv_ava"]["Dim. 2"].values[
        0
    ]

    ax1.arrow(
        x1,
        y1,
        x2 - x1,
        y2 - y1,
        width=0.01,
        color="magenta",
        head_length=0.0,
        head_width=0.0,
    )
    ax1.set_title("Original among Avatar values")

    sns.scatterplot(
        ax=ax2,
        x="Dim. 1",
        y="Dim. 2",
        data=ori_to_ava,
        hue="Type",
        alpha=0.7,
        palette=dict(
            Avatar=PRIMARY_GREEN,
            Original=PRIMARY_GREY,
        ),
    )
    ax2.set_xlim(
        total_coordinates["Dim. 1"].min() - 0.2, total_coordinates["Dim. 1"].max() + 0.2
    )
    ax2.set_ylim(
        total_coordinates["Dim. 2"].min() - 0.2, total_coordinates["Dim. 2"].max() + 0.2
    )

    x1 = ori_to_ava.loc[ori_to_ava["Type"] == "Original"]["Dim. 1"].values[0]
    x2 = ori_to_ava.loc[ori_to_ava["Type"] == "Avatar"]["Dim. 1"].values[0]
    y1 = ori_to_ava.loc[ori_to_ava["Type"] == "Original"]["Dim. 2"].values[0]
    y2 = ori_to_ava.loc[ori_to_ava["Type"] == "Avatar"]["Dim. 2"].values[0]

    ax2.arrow(
        x1,
        y1,
        x2 - x1,
        y2 - y1,
        width=0.01,
        color=PRIMARY_GREEN,
        head_length=0.0,
        head_width=0.0,
    )
    ax2.set_title("Original to Avatar travel")

    return fig
