PRIMARY_GREY = "dimgrey"
PRIMARY_GREEN = "#3BD6B0"
PRIMARY_CT_GAN = "#4db7fd"
PRIMARY_STRUCT = "#b03bd6"

PROGRESSIVE_PALETTE = [
    "#ffffff",
    "#fdfffe",
    "#fbfefd",
    "#f9fefd",
    "#f7fdfc",
    "#f5fdfb",
    "#f3fdfa",
    "#f1fcf9",
    "#effcf9",
    "#edfbf8",
    "#ebfbf7",
    "#e9faf6",
    "#e7faf6",
    "#e6faf5",
    "#e4f9f4",
    "#e2f9f3",
    "#e0f8f2",
    "#def8f2",
    "#dcf8f1",
    "#daf7f0",
    "#d8f7ef",
    "#d6f6ee",
    "#d4f6ee",
    "#d2f6ed",
    "#d0f5ec",
    "#cef5eb",
    "#ccf4ea",
    "#caf4ea",
    "#c8f4e9",
    "#c6f3e8",
    "#c4f3e7",
    "#c2f2e7",
    "#c0f2e6",
    "#bef1e5",
    "#bcf1e4",
    "#baf1e3",
    "#b8f0e3",
    "#b6f0e2",
    "#b5efe1",
    "#b3efe0",
    "#b1efdf",
    "#afeedf",
    "#adeede",
    "#abeddd",
    "#a9eddc",
    "#a7eddb",
    "#a5ecdb",
    "#a3ecda",
    "#a1ebd9",
    "#9febd8",
    "#9debd8",
    "#9bead7",
    "#99ead6",
    "#97e9d5",
    "#95e9d4",
    "#93e8d4",
    "#91e8d3",
    "#8fe8d2",
    "#8de7d1",
    "#8be7d0",
    "#89e6d0",
    "#87e6cf",
    "#85e6ce",
    "#84e5cd",
    "#82e5cc",
    "#80e4cc",
    "#7ee4cb",
    "#7ce4ca",
    "#7ae3c9",
    "#78e3c8",
    "#76e2c8",
    "#74e2c7",
    "#72e1c6",
    "#70e1c5",
    "#6ee1c5",
    "#6ce0c4",
    "#6ae0c3",
    "#68dfc2",
    "#66dfc1",
    "#64dfc1",
    "#62dec0",
    "#60debf",
    "#5eddbe",
    "#5cddbd",
    "#5addbd",
    "#58dcbc",
    "#56dcbb",
    "#54dbba",
    "#53dbb9",
    "#51dbb9",
    "#4fdab8",
    "#4ddab7",
    "#4bd9b6",
    "#49d9b6",
    "#47d8b5",
    "#45d8b4",
    "#43d8b3",
    "#41d7b2",
    "#3fd7b2",
    "#3dd6b1",
]


DIVERGENT_PALETTE = [
    "#de6281",
    "#df6584",
    "#df6886",
    "#e06b89",
    "#e16f8b",
    "#e1728e",
    "#e27590",
    "#e37893",
    "#e37b95",
    "#e47e98",
    "#e5819a",
    "#e5859d",
    "#e6889f",
    "#e78ba2",
    "#e78ea4",
    "#e891a7",
    "#e994a9",
    "#e997ac",
    "#ea9bae",
    "#eb9eb1",
    "#eba1b3",
    "#eca4b6",
    "#eda7b8",
    "#edaabb",
    "#eeadbd",
    "#eeb0c0",
    "#efb4c3",
    "#f0b7c5",
    "#f0bac8",
    "#f1bdca",
    "#f2c0cd",
    "#f2c3cf",
    "#f3c6d2",
    "#f4cad4",
    "#f4cdd7",
    "#f5d0d9",
    "#f6d3dc",
    "#f6d6de",
    "#f7d9e1",
    "#f8dce3",
    "#f8e0e6",
    "#f9e3e8",
    "#fae6eb",
    "#fae9ed",
    "#fbecf0",
    "#fceff2",
    "#fcf2f5",
    "#fdf6f7",
    "#fef9fa",
    "#fefcfc",
    "#ffffff",
    "#fbfefd",
    "#f7fdfc",
    "#f3fdfa",
    "#effcf9",
    "#ebfbf7",
    "#e7faf6",
    "#e4f9f4",
    "#e0f8f2",
    "#dcf8f1",
    "#d8f7ef",
    "#d4f6ee",
    "#d0f5ec",
    "#ccf4ea",
    "#c8f4e9",
    "#c4f3e7",
    "#c0f2e6",
    "#bcf1e4",
    "#b8f0e3",
    "#b5efe1",
    "#b1efdf",
    "#adeede",
    "#a9eddc",
    "#a5ecdb",
    "#a1ebd9",
    "#9debd8",
    "#99ead6",
    "#95e9d4",
    "#91e8d3",
    "#8de7d1",
    "#89e6d0",
    "#85e6ce",
    "#82e5cc",
    "#7ee4cb",
    "#7ae3c9",
    "#76e2c8",
    "#72e1c6",
    "#6ee1c5",
    "#6ae0c3",
    "#66dfc1",
    "#62dec0",
    "#5eddbe",
    "#5addbd",
    "#56dcbb",
    "#53dbb9",
    "#4fdab8",
    "#4bd9b6",
    "#47d8b5",
    "#43d8b3",
    "#3fd7b2",
]

CATEGORICAL_PALETTE = [
    "#6fe7dd",
    "#ff7070",
    "#3490de",
    "#fbb901",
    "#6639a6",
    "#e74c3c",
    "#521262",
    "#e8a7c3",
    "#76f8cf",
    "#e7fa25",
    "#d86eff",
    "#89c4ff",
    "#fa4659",
    "#2eb872",
    "#7e6752",
    "#a40a3c",
]

DIVERGENT_CTGAN = ['#de6281',
'#df6584',
'#df6886',
'#e06b89',
'#e16f8b',
'#e1728e',
'#e27590',
'#e37893',
'#e37b95',
'#e47e98',
'#e5819a',
'#e5859d',
'#e6889f',
'#e78ba2',
'#e78ea4',
'#e891a7',
'#e994a9',
'#e997ac',
'#ea9bae',
'#eb9eb1',
'#eba1b3',
'#eca4b6',
'#eda7b8',
'#edaabb',
'#eeadbd',
'#eeb0c0',
'#efb4c3',
'#f0b7c5',
'#f0bac8',
'#f1bdca',
'#f2c0cd',
'#f2c3cf',
'#f3c6d2',
'#f4cad4',
'#f4cdd7',
'#f5d0d9',
'#f6d3dc',
'#f6d6de',
'#f7d9e1',
'#f8dce3',
'#f8e0e6',
'#f9e3e8',
'#fae6eb',
'#fae9ed',
'#fbecf0',
'#fceff2',
'#fcf2f5',
'#fdf6f7',
'#fef9fa',
'#fefcfc',
'#ffffff',
'#fbfeff',
'#f8fcff',
'#f4fbff',
'#f1f9ff',
'#edf8ff',
'#eaf6ff',
'#e6f5ff',
'#e3f3ff',
'#dff2ff',
'#dbf1ff',
'#d8efff',
'#d4eeff',
'#d1ecfe',
'#cdebfe',
'#cae9fe',
'#c6e8fe',
'#c2e7fe',
'#bfe5fe',
'#bbe4fe',
'#b8e2fe',
'#b4e1fe',
'#b1dffe',
'#addefe',
'#aadcfe',
'#a6dbfe',
'#a2dafe',
'#9fd8fe',
'#9bd7fe',
'#98d5fe',
'#94d4fe',
'#91d2fe',
'#8dd1fe',
'#8acffe',
'#86cefe',
'#82cdfe',
'#7fcbfe',
'#7bcafe',
'#78c8fd',
'#74c7fd',
'#71c5fd',
'#6dc4fd',
'#69c3fd',
'#66c1fd',
'#62c0fd',
'#5fbefd',
'#5bbdfd',
'#58bbfd',
'#54bafd',
'#51b8fd']

DIVERGENT_STRUCT = ['#de6281',
'#df6584',
'#df6886',
'#e06b89',
'#e16f8b',
'#e1728e',
'#e27590',
'#e37893',
'#e37b95',
'#e47e98',
'#e5819a',
'#e5859d',
'#e6889f',
'#e78ba2',
'#e78ea4',
'#e891a7',
'#e994a9',
'#e997ac',
'#ea9bae',
'#eb9eb1',
'#eba1b3',
'#eca4b6',
'#eda7b8',
'#edaabb',
'#eeadbd',
'#eeb0c0',
'#efb4c3',
'#f0b7c5',
'#f0bac8',
'#f1bdca',
'#f2c0cd',
'#f2c3cf',
'#f3c6d2',
'#f4cad4',
'#f4cdd7',
'#f5d0d9',
'#f6d3dc',
'#f6d6de',
'#f7d9e1',
'#f8dce3',
'#f8e0e6',
'#f9e3e8',
'#fae6eb',
'#fae9ed',
'#fbecf0',
'#fceff2',
'#fcf2f5',
'#fdf6f7',
'#fef9fa',
'#fefcfc',
'#ffffff',
'#fdfbfe',
'#fcf7fd',
'#faf3fd',
'#f9effc',
'#f7ebfb',
'#f6e7fa',
'#f4e4f9',
'#f2e0f8',
'#f1dcf8',
'#efd8f7',
'#eed4f6',
'#ecd0f5',
'#eaccf4',
'#e9c8f4',
'#e7c4f3',
'#e6c0f2',
'#e4bcf1',
'#e3b8f0',
'#e1b5ef',
'#dfb1ef',
'#deadee',
'#dca9ed',
'#dba5ec',
'#d9a1eb',
'#d89deb',
'#d699ea',
'#d495e9',
'#d391e8',
'#d18de7',
'#d089e6',
'#ce85e6',
'#cc82e5',
'#cb7ee4',
'#c97ae3',
'#c876e2',
'#c672e1',
'#c56ee1',
'#c36ae0',
'#c166df',
'#c062de',
'#be5edd',
'#bd5add',
'#bb56dc',
'#b953db',
'#b84fda',
'#b64bd9',
'#b547d8',
'#b343d8',
'#b23fd7']