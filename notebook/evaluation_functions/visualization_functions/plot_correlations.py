from typing import Tuple, List

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy.typing import NDArray


from evaluation_functions.visualization_functions.color_palette import DIVERGENT_PALETTE, PROGRESSIVE_PALETTE


def correlation_plot(
    original: pd.DataFrame,
    synthetic: pd.DataFrame,
    difference: bool = False,
    figsize: Tuple[int, int] = (14, 8),
    palette: List[str] = DIVERGENT_PALETTE,
    name: str = 'Synthetic',
) -> matplotlib.figure.Figure:
    """Compare correlations between an original variable and its synthetized equivalent.

    Arguments
    ---------
        original: correlation matrix of original continuous variables
        synthetic: correlation matrix of synthetic continuous variables
        difference:  boolean value indicating if the difference of correlation is computed
        figsize: tuple specifying figure size

    Returns
    -------
        graph: a comparison of correlation matrices
    """
    if difference:
        corr_diff = abs(original - synthetic).round(2)

        # Fix plot size and shape
        plt.subplots(ncols=1, figsize=figsize)

        # Generate a mask for the upper triangle
        mask: NDArray[np.bool_] = np.triu(np.ones_like(corr_diff, dtype=bool))

        # Generate a custom diverging colormap
        cmap = sns.color_palette(PROGRESSIVE_PALETTE, n_colors=30, as_cmap=True)

        # Draw the heatmap with the mask and correct aspect ratio
        plot = sns.heatmap(
            corr_diff,
            mask=mask,
            cmap=cmap,
            annot=False,
            vmax=1,
            vmin=0,
            square=True,
            linewidths=0.5,
            cbar_kws={"shrink": 0.5},
        )
        plot.set_title("Correlation difference matrix", fontsize=16)
        return plot

    # Fix plot size and shape
    fig = plt.figure(figsize=figsize)

    # Generate a mask for the upper triangle
    mask1: NDArray[np.bool_] = np.triu(np.ones_like(original, dtype=bool))
    mask2: NDArray[np.bool_] = np.triu(np.ones_like(synthetic, dtype=bool))

    # Generate a custom diverging colormap
    cmap = sns.color_palette(palette, n_colors=30, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    ax1 = fig.add_subplot(121)
    sns.heatmap(
        original.round(2),
        mask=mask1,
        cmap=cmap,
        center=0,
        annot=False,
        vmax=1,
        vmin=-1,
        square=True,
        linewidths=0.5,
        cbar_kws={"shrink": 0.5},
    )
    ax1.set_title("ORIGINAL", fontsize=10)

    ax2 = fig.add_subplot(122)
    sns.heatmap(
        synthetic.round(2),
        mask=mask2,
        cmap=cmap,
        center=0,
        annot=False,
        vmax=1,
        vmin=-1,
        square=True,
        linewidths=0.5,
        cbar_kws={"shrink": 0.5},
    )
    ax2.set_title(name, fontsize=10)

    fig.suptitle(f"Original vs {name} correlation matrix comparison", fontsize=15, y=0.9)
    fig.tight_layout()
    return fig
