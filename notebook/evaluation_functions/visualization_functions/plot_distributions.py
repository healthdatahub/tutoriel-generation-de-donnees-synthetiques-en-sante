from pathlib import Path
from typing import Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.patches import Patch
from numpy.typing import NDArray

from evaluation_functions.utils.get_numeric_dtypes import NUMERIC_DTYPES
from evaluation_functions.visualization_functions.color_palette import PRIMARY_GREEN, PRIMARY_GREY

# This document gather several visualization function useful to evaluate
# signal retention after avatarization

# Single plots

# single plots are automatically displayed and a AxesSubplot object is return
# if you want to apply some modifications to display the graph you affected as
# 'plot' simply do plot.figure

# Double plots

# Figures with 2 panels are also automatically displayed but returned this time
# as Figure class object.  to display the figure you affected as 'plot' simply
# do plot.


def distribution_plot(
    variable_name: str,
    records: pd.DataFrame,
    synthetic: pd.DataFrame,
    synthetic_color: str =PRIMARY_GREEN,
    figsize: Tuple[int, int] = (14, 8),
    style: str = "whitegrid",
) -> matplotlib.figure.Figure:
    """Compare distributions between an original variable and its avatarized equivalent.

    Arguments
    ---------
        variable_name: string referring to colnames used for comparison
        records: dataframe of original values
        synthetic: dataframe of avatarized values
        figsize: vector of two int specifying figure size
        style: string specifying seaborn theme used

    Returns
    -------
        graph: a seaborn graph comparing two distributions
    """
    fig, ax = plt.subplots(figsize=figsize)
    sns.set_style(style)

    if records[variable_name].dtype != synthetic[variable_name].dtype:
        raise ValueError("records and synthetic should have the same data type")

    title = f"Comparative distribution of {variable_name} variable"
    if records[variable_name].dtype in NUMERIC_DTYPES:
        plot_continuous_distributions(
            records=records[variable_name],
            synthetic=synthetic[variable_name],
            ax=ax,
            title=title,
            synthetic_color=synthetic_color,
        )
        return fig

    if records[variable_name].dtype == "datetime64[ns]":
        plot_date_distributions(
            records=records[variable_name],
            synthetic=synthetic[variable_name],
            ax=ax,
            title=title,
            synthetic_color=synthetic_color,
        )
        return fig

    # Categorical data

    combined = pd.concat([records, synthetic])
    x: NDArray[np.str_] = np.array(["Original", "Synthetic"])
    combined["type"] = np.repeat(x, [records.shape[0], synthetic.shape[0]], axis=0)
    sns.countplot(
        ax=ax,
        x=variable_name,
        hue="type",
        data=combined,
        palette=dict(Original=PRIMARY_GREY, Synthetic=synthetic_color),
    )
    ax.set_title(f"Comparative modality count of {variable_name} variable")

    return fig


def plot_date_distributions(
    records: pd.Series, synthetic: pd.Series, ax: matplotlib.axes.Axes, title: str, synthetic_color: str = PRIMARY_GREEN
) -> matplotlib.axes.Axes:

    # Only works on a day by day basis:
    # Return proleptic Gregorian ordinal. January 1 of year 1 is day 1.
    # https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.toordinal.html
    x: NDArray[np.int_] = np.array([x.toordinal() for x in records.dropna()])
    y: NDArray[np.int_] = np.array([x.toordinal() for x in synthetic.dropna()])

    sns.kdeplot(ax=ax, data=x, color=PRIMARY_GREY)
    sns.kdeplot(ax=ax, data=y, color=synthetic_color)

    x_ticks = ax.get_xticks()  # Getting x_ticks before ax.vlines() is necessary

    # Draw lines representing the median of the dataset.
    plot_lines = ax.get_lines()
    records_line_x, records_line_y = (
        plot_lines[0].get_xdata(),
        plot_lines[0].get_ydata(),
    )
    synthetic_line_x, synthetic_line_y = (
        plot_lines[1].get_xdata(),
        plot_lines[1].get_ydata(),
    )

    # We need to use percentile and not median here, otherwise we get strange datetimes.
    median_records = np.percentile(x, 50)
    median_synthetic = np.percentile(y, 50)

    ax.set_title(title)

    ax.vlines(
        median_records,
        0,
        np.interp(median_records, records_line_x, records_line_y),
        color=PRIMARY_GREY,
        ls=":",
    )

    ax.vlines(
        median_synthetic,
        0,
        np.interp(median_synthetic, synthetic_line_x, synthetic_line_y),
        color=synthetic_color,
        ls=":",
    )

    xlabels = [pd.Timestamp.fromordinal(int(x)).strftime("%Y-%m-%d") for x in x_ticks]
    ax.set_xticklabels(xlabels, rotation=45, horizontalalignment="right")

    sorted_records = records.sort_values()
    sorted_synthetic = synthetic.sort_values()
    middle_index = records.shape[0] // 2
    legend_elements = [
        Patch(
            facecolor=PRIMARY_GREY,
            edgecolor="black",
            label=f"median = {sorted_records[middle_index].strftime('%Y-%m-%d')}",
        ),
        Patch(
            facecolor=synthetic_color,
            edgecolor="black",
            label=f"median = {sorted_synthetic[middle_index].strftime('%Y-%m-%d') }",
        ),
    ]
    ax.legend(handles=legend_elements, loc="upper right")

    return ax


def plot_continuous_distributions(
    records: pd.Series, synthetic: pd.Series, ax: matplotlib.axes.Axes, title: str, synthetic_color: str = PRIMARY_GREEN
) -> matplotlib.axes.Axes:

    sns.kdeplot(ax=ax, data=records, color=PRIMARY_GREY)

    sns.kdeplot(ax=ax, data=synthetic, color=synthetic_color)

    # Draw lines representing the median of the dataset.
    plot_lines = ax.get_lines()
    records_line_x, records_line_y = (
        plot_lines[0].get_xdata(),
        plot_lines[0].get_ydata(),
    )
    synthetic_line_x, synthetic_line_y = (
        plot_lines[1].get_xdata(),
        plot_lines[1].get_ydata(),
    )
    median_records = np.median(records.dropna())
    median_synthetic = np.median(synthetic.dropna())

    ax.vlines(
        median_records,
        0,
        np.interp(median_records, records_line_x, records_line_y),
        color=PRIMARY_GREY,
        ls=":",
    )

    ax.vlines(
        median_synthetic,
        0,
        np.interp(median_synthetic, synthetic_line_x, synthetic_line_y),
        color=synthetic_color,
        ls=":",
    )

    # The first median line might get drawn over by the second one
    # if the median is exactly equal.

    legend_elements = [
        Patch(
            facecolor=PRIMARY_GREY,
            edgecolor="black",
            label=f"median = {round(median_records)}",
        ),
        Patch(
            facecolor=PRIMARY_GREY,
            edgecolor="black",
            label=f"sd = {round(np.std(records))}",
        ),
        Patch(
            facecolor=synthetic_color,
            edgecolor="black",
            label=f"median = {round(median_synthetic)}",
        ),
        Patch(
            facecolor=synthetic_color,
            edgecolor="black",
            label=f"sd = {round(np.std(synthetic))}",
        ),
    ]

    ax.set_title(title)
    ax.legend(handles=legend_elements, loc="upper right")
    return ax


def cumulative_distribution_plot(
    variable_name: str,
    records: pd.DataFrame,
    synthetic: pd.DataFrame,
    synthetic_color: str = PRIMARY_GREEN,
    figsize: Tuple[int, int] = (14, 8),
    style: str = "whitegrid",
) -> matplotlib.figure.Figure:
    """Compare original and synthetic cumulative continuous distributions.

    Arguments
    ---------
        variable_name: string referring to colnames used for comparison
        records: dataframe of original values
        synthetic: dataframe of avatarized values
        figsize: vector of two int specifying figure size
        style: string specifying seaborn theme used

    Returns
    -------
        graph: a seaborn comparison graph of cumulative distributions
    """
    fig, ax = plt.subplots(figsize=figsize)
    sns.set_style(style)

    if records[variable_name].dtype != synthetic[variable_name].dtype:
        raise ValueError("records and synthetic should have the same data type")

    sns.ecdfplot(ax=ax, data=records, x=variable_name, color=PRIMARY_GREY)
    sns.ecdfplot(ax=ax, data=synthetic, x=variable_name, color=synthetic_color)
    ax.set_title(f"Comparative cumulative distribution of {variable_name} variable")
    legend_elements = [
        Patch(facecolor=PRIMARY_GREY, edgecolor="black", label="Original"),
        Patch(facecolor=synthetic_color, edgecolor="black", label="Synthetic"),
    ]
    ax.legend(handles=legend_elements, loc="upper left")

    return fig


if __name__ == "__main__":
    records_file = Path(__file__) / "../../../../fixtures/iris.csv"
    syntheticfile = Path(__file__) / "../../../../fixtures/iris_synthetic.csv"
    records = pd.read_csv(records_file.resolve())
    synthetic = pd.read_csv(syntheticfile.resolve())

    records["time"] = pd.date_range(
        start="1/1/2018", end="1/08/2018", periods=records.shape[0]
    )
    synthetic["time"] = pd.date_range(
        start="1/1/2018", end="1/08/2018", periods=synthetic.shape[0]
    )

    distribution_plot(variable_name="sepal.length", records=records, synthetic=synthetic)
    distribution_plot(variable_name="variety", records=records, synthetic=synthetic)
    distribution_plot(variable_name="time", records=records, synthetic=synthetic)
    cumulative_distribution_plot(
        variable_name="sepal.length", records=records, synthetic=synthetic
    )
    cumulative_distribution_plot(
        variable_name="variety", records=records, synthetic=synthetic
    )
    cumulative_distribution_plot(variable_name="time", records=records, synthetic=synthetic)
    plt.show()
