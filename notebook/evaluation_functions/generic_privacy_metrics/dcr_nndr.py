from typing import List, NamedTuple

import numpy as np
import pandas as pd

from evaluation_functions.specific_privacy_metrics.distance import get_distances_closest_records



class DistanceToClosestRecord(NamedTuple):
    """Store the results of distance metrics for intuitive access.

    Returns
    -------
        median: median of all distances of closest record
        values: distance values between each synthetic individual and its closest record
    """

    median: np.float_
    values: List[float]


class ClosestRecordsDistancesRatio(NamedTuple):
    """Store the results of distances ratio for intuitive access.

    Returns
    -------
        median: median of all nearest records distances ratio
        values: nearest records distances ratio for all synthetic data
    """

    median: np.float_
    values: List[float]


def get_distance_to_closest(
    original_coordinates: pd.DataFrame, avatar_coordinates: pd.DataFrame
) -> DistanceToClosestRecord:
    """Get summary of distance to closest record."""
    distance_to_closest = _get_distance_to_closest(
        original_coordinates, avatar_coordinates
    )
    results = DistanceToClosestRecord(
        np.median(distance_to_closest),
        distance_to_closest,
    )
    return results


def get_closest_distances_ratio(
    original_coordinates: pd.DataFrame, avatar_coordinates: pd.DataFrame
) -> ClosestRecordsDistancesRatio:
    """Get summary of closest records distances ratio."""
    closest_distances_ratio = _get_closest_distances_ratio(
        original_coordinates, avatar_coordinates
    )
    results = ClosestRecordsDistancesRatio(
        np.median(closest_distances_ratio),
        closest_distances_ratio,
    )
    return results


def _get_distance_to_closest(
    original_coordinates: pd.DataFrame, avatar_coordinates: pd.DataFrame
) -> List[float]:
    """Get distance to the closest record.

    Distance to closest is the distance from each synthetic record to its closest original record.
    """
    indices_distances = get_distances_closest_records(
        np.ascontiguousarray(original_coordinates), np.ascontiguousarray(avatar_coordinates), searching_frame=1
    )
    _, distances = zip(*indices_distances)
    return [distance[0] for distance in distances]


def _get_closest_distances_ratio(
    original_coordinates: pd.DataFrame, avatar_coordinates: pd.DataFrame
) -> List[float]:
    """Get closest records distances ratio.

    Closest distances ratio is the ratio of the distance of each synthetic record to its closest
    and second closest record in the original set.
    """
    indices_distances = get_distances_closest_records(
        np.ascontiguousarray(original_coordinates), np.ascontiguousarray(avatar_coordinates), searching_frame=2
    )
    _, distances = zip(*indices_distances)

    ratio = [
        1 if distance[1] == 0 else distance[0] / distance[1] for distance in distances
    ]
    return ratio
