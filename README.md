# Tutoriel - Génération de données synthétiques en santé

Notebook pédagogique présentant les enjeux des données synthétiques en santé, et différentes approches visant à générer de telles données. Ce notebook est développé collaborativement par Octopize et le Health Data Hub.


## Prérequis

Installer [Poetry](https://python-poetry.org/) (un gestionnaire de package python).


## Installation
Pour installer le projet, lancez:
```bash
make install
```
(NOTE: cette commande suppose que `make` est installé.)

## Structure

```bash
build/      # données originales issue de la base de données
data/
    base/                 # original MIMIC III base de donnée depuis https://physionet.org/content/mimiciii-demo/1.4/
    original/             # flat_data imputé et non-imputé
    synthetic/            # données synthétiques 
    synthetic_data_struct/   # ressources pour la génération des données synthétiques HDH
notebooks/  # analyse de données
static/     # fichiers statiques 
```

## Utilisation
[main.ipynb](notebook/main.ipynb) présente la comparaison des méthodes de génération de données synthétiques sur la base de la conservation de l'utilité et du respect de la confidentialité. Le notebook peut être lancé de façon indépendante et repose sur les données et les fonctions accessibles sur cet espace.

## Auteurs

Aymeric Floyrac (HDH)  
Camille Mosser (HDH)  
Julien Petot (Octopize)  
Morgan Guillaudeux (Octopize)  
Romain Girard (HDH)

## Licence
Apache 2.0
