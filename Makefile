SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

install:  ## Install the stack
	poetry install
.PHONY: install

notebook:  ## Run the notebook
	poetry run jupyter notebook
.PHONY: notebook

py: ## Generate or update .py from .ipynb
	poetry run jupytext --set-formats ipynb,py  **/*.ipynb 
.PHONY: py

##@ Tests

lci: py lint-fix ci   ## Autofix then run CI and generate py file from ipynb
.PHONY: lci

ci: lint typecheck  ## Run all checks
.PHONY: ci

lint:  ## Run linting
	poetry run flake8 notebook
.PHONY: lint

lint-fix:  ## Run autoformatters
	poetry run black notebook
	poetry run isort notebook
.PHONY: lint-fix

typecheck:  ## Run typechecking
	poetry run mypy --show-error-codes --pretty notebook
.PHONY: typecheck


.DEFAULT_GOAL := help
help: Makefile
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[\/\.a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
